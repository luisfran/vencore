-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 06, 2018 at 12:23 AM
-- Server version: 5.7.21
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `vencore`
--

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(300) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`) VALUES
(1, 'Apartamento'),
(2, 'Casa'),
(3, 'Terreno'),
(4, 'Finca'),
(5, 'Rancho de Playa'),
(6, 'Otro');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('9f8ll02ht9f0g9gm57rhusu37n585131', '::1', 1544050823, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343035303832333b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('8dmunau10tggjrk3epk39k88q0re7pkp', '::1', 1544050228, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343035303232383b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('jp5rq71ag8e5j4uscesujhgvs3g58e5n', '::1', 1544049048, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343034393034383b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('o8j1q4febc4t0iceete7f33t8nu7pjbp', '::1', 1544048731, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343034383733313b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('s0v7c7t635999mdq5m2dsksrknvg0g2v', '::1', 1544045249, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343034353234393b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('piihv3v8oha7qa6seebpvhp9rp3t6ppm', '::1', 1544047638, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343034373633383b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('a7vnpatmjvefpcqjpd36curjctqubikd', '::1', 1544047978, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343034373937383b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('4qoo6m7l8ob6jit27h7tssvf712c98j9', '::1', 1544048407, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343034383430373b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('gs9ctklpb4cv28802fm73d1jfv71voiu', '::1', 1544054355, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343035343335353b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('n49f0jspb0ksf89numstp573aegro6ef', '::1', 1544054629, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343035343335353b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('91tf9osu9li5ss2t24h3akveg2eiqs90', '::1', 1544052047, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343035323034373b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('iepfetqps847mm81f0pgr8pjjdacqrpp', '::1', 1544053285, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343035333238353b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('jrf17eo9vhi5464eg54h6v03m41nhgg9', '::1', 1544051444, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343035313434343b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('7d8ed1hakv6bt3u9urlklt1r1jcvo939', '::1', 1544051745, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343035313734353b76656e636f72655f6163636573737c733a353a2276616c6964223b),
('p5sdlvn1napak4qlnjtvh24o1p0rc80q', '::1', 1544051131, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534343035313133313b76656e636f72655f6163636573737c733a353a2276616c6964223b);

-- --------------------------------------------------------

--
-- Table structure for table `departamento`
--

CREATE TABLE `departamento` (
  `id` int(11) NOT NULL,
  `id_zona` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departamento`
--

INSERT INTO `departamento` (`id`, `id_zona`, `nombre`) VALUES
(1, 4, 'Ahuachapan'),
(2, 4, 'Santa Ana'),
(3, 4, 'Sonsonate'),
(4, 1, 'Chalatenango'),
(5, 1, 'La Libertad'),
(6, 1, 'San Salvador'),
(7, 1, 'Cuscatlán'),
(8, 2, 'La Paz'),
(9, 2, 'Cabañas'),
(10, 2, 'San Vicente'),
(11, 3, 'Usulután'),
(12, 3, 'San Miguel'),
(13, 3, 'Morazán'),
(14, 3, 'La unión'),
(15, 1, 'No proporcionado');

-- --------------------------------------------------------

--
-- Table structure for table `fotos`
--

CREATE TABLE `fotos` (
  `id` int(11) NOT NULL,
  `url` text NOT NULL,
  `size` int(11) NOT NULL,
  `id_vivienda` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fotos`
--

INSERT INTO `fotos` (`id`, `url`, `size`, `id_vivienda`) VALUES
(16, 'casa6.jpeg', 9437, 9),
(13, 'casa5.jpeg', 10720, 5),
(14, 'casa6.jpeg', 9437, 6),
(11, 'casa1.jpeg', 8872, 8),
(15, 'casa1.jpeg', 8872, 7),
(12, 'casa2.jpeg', 9068, 8);

-- --------------------------------------------------------

--
-- Table structure for table `municipio`
--

CREATE TABLE `municipio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `id_departamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `municipio`
--

INSERT INTO `municipio` (`id`, `nombre`, `id_departamento`) VALUES
(1, 'Ahuachapán', 1),
(2, 'Jujutla', 1),
(3, 'Atiquizaya', 1),
(4, 'Concepción de Ataco', 1),
(5, 'El Refugio', 1),
(6, 'Guaymango', 1),
(7, 'Apaneca', 1),
(8, 'San Francisco Menéndez', 1),
(9, 'San Lorenzo', 1),
(10, 'San Pedro Puxtla', 1),
(11, 'Tacuba', 1),
(12, 'Turín', 1),
(13, 'Acajutla', 3),
(14, 'Armenia', 3),
(15, 'Caluco', 3),
(16, 'Cuisnahuat', 3),
(17, 'Izalco', 3),
(18, 'Juayúa', 3),
(19, 'Nahulingo', 3),
(20, 'Salcoatitán', 3),
(21, 'San Antonio del Monte', 3),
(22, 'Santa Catarina Masahuat', 3),
(23, 'Santa Isabel Ishuatán', 3),
(24, 'Santo Domingo de Guzman', 3),
(25, 'Sonsonate', 3),
(26, 'Sonzacate', 3),
(27, 'Nahuizalco', 3),
(28, 'San Julián', 3),
(29, 'Agua Caliente', 4),
(30, 'Arcatao', 4),
(31, 'Azacualpa', 4),
(32, 'Chalatenango', 4),
(33, 'Citalá', 4),
(34, 'Comalapa', 4),
(35, 'Concepción Quezaltepeque', 4),
(36, 'Dulce Nombre de María', 4),
(37, 'El Carrizal', 4),
(38, 'El Paraíso', 4),
(39, 'La Laguna', 4),
(40, 'La Palma', 4),
(41, 'La Reina', 4),
(42, 'Las Vueltas', 4),
(43, 'Nueva Concepción', 4),
(44, 'Nueva Trinidad', 4),
(45, 'Nombre de Jesús', 4),
(46, 'Ojos de Agua', 4),
(47, 'Potonico', 4),
(48, 'San Antonio de la Cruz', 4),
(49, 'San Antonio Los Ranchos', 4),
(50, 'San Fernando', 4),
(51, 'San Francisco Lempa', 4),
(52, 'San Francisco Morazán', 4),
(53, 'San Ignacio', 4),
(54, 'San Isidro Labrador', 4),
(55, 'San José Cancasque', 4),
(56, 'San José Las Flores', 4),
(57, 'San Luis del Carmen', 4),
(58, 'San Miguel de Mercedes', 4),
(59, 'San Rafael', 4),
(60, 'Santa Rita', 4),
(61, 'Tejutla', 4),
(62, 'Candelaria de la Frontera', 2),
(63, 'Chalchuapa', 2),
(64, 'Coatepeque', 2),
(65, 'El Congo', 2),
(66, 'El Porvenir', 2),
(67, 'Masahuat', 2),
(68, 'Metapán', 2),
(69, 'San Antonio Pajonal', 2),
(70, 'San Sebastián Salitrillo', 2),
(71, 'Santa Ana', 2),
(72, 'Santa Rosa Guachipilín', 2),
(73, 'Santiago de la Frontera', 2),
(74, 'Texistepeque', 2),
(75, 'Antiguo Cuscatlán', 5),
(76, 'Chiltiupán', 5),
(77, 'Ciudad Arce', 5),
(78, 'Colón', 5),
(79, 'Comasagua', 5),
(80, 'Huizúcar', 5),
(81, 'Jayaque', 5),
(82, 'Jicalapa', 5),
(83, 'La Libertad', 5),
(84, 'Santa Tecla', 5),
(85, 'Nuevo Cuscatlán', 5),
(86, 'San Juan Opico', 5),
(87, 'Quezaltepeque', 5),
(88, 'Sacacoyo', 5),
(89, 'San José Villanueva', 5),
(90, 'San Matías', 5),
(91, 'San Pablo Tacachico', 5),
(92, 'Talnique', 5),
(93, 'Tamanique', 5),
(94, 'Teotepeque', 5),
(95, 'Tepecoyo', 5),
(96, 'Zaragoza', 5),
(97, 'Aguilares', 6),
(98, 'Apopa', 6),
(99, 'Ayutuxtepeque', 6),
(100, 'Cuscatancingo', 6),
(101, 'Ciudad Delgado', 6),
(102, 'El Paisnal', 6),
(103, 'Guazapa', 6),
(104, 'Ilopango', 6),
(105, 'Mejicanos', 6),
(106, 'Nejapa', 6),
(107, 'Panchimalco', 6),
(108, 'Rosario de Mora', 6),
(109, 'San Marcos', 6),
(110, 'San Martín', 6),
(111, 'San Salvador', 6),
(112, 'Santiago Texacuangos', 6),
(113, 'Santo Tomás', 6),
(114, 'Soyapango', 6),
(115, 'Candelaria', 7),
(116, 'Cojutepeque', 7),
(117, 'El Carmen', 7),
(118, 'El Rosario', 7),
(119, 'Monte San Juan', 7),
(120, 'Oratorio de Concepción', 7),
(121, 'San Bartolomé Perulapía', 7),
(122, 'San Cristóbal', 7),
(123, 'San José Guayabal', 7),
(124, 'San Pedro Perulapán', 7),
(125, 'San Rafael Cedros', 7),
(126, 'San Ramón', 7),
(127, 'Santa Cruz Analquito', 7),
(128, 'Santa Cruz Michapa', 7),
(129, 'Suchitoto', 7),
(130, 'Tenancingo', 7),
(131, 'Cuyultitán', 8),
(132, 'Rosario de la Paz', 8),
(133, 'Jerusalén', 8),
(134, 'Mercedes La Ceiba', 8),
(135, 'Olocuilta', 8),
(136, 'Paraíso de Osorio', 8),
(137, 'San Antonio Masahuat', 8),
(138, 'San Emigdio', 8),
(139, 'San Francisco Chinameca', 8),
(140, 'San Juan Nonualco', 8),
(141, 'San Juan Talpa', 8),
(142, 'San Juan Tepezontes', 8),
(143, 'San Luis Talpa', 8),
(144, 'San Luis La Herradura', 8),
(145, 'San Miguel Tepezontes', 8),
(146, 'San Pedro Masahuat', 8),
(147, 'San Pedro Nonualco', 8),
(148, 'San Rafael Obrajuelo', 8),
(149, 'Santa María Ostuma', 8),
(150, 'Santiago Nonualco', 8),
(151, 'Tapalhuaca', 8),
(152, 'Zacatecoluca', 8),
(153, 'Cinquera', 9),
(154, 'Villa Dolores', 9),
(155, 'Guacotecti', 9),
(156, 'Ilobasco', 9),
(157, 'Jutiapa', 9),
(158, 'San Isidro', 9),
(159, 'Sensuntepeque', 9),
(160, 'Tejutepeque', 9),
(161, 'Villa Victoria', 9),
(162, 'Apastepeque', 10),
(163, 'Guadalupe', 10),
(164, 'San Cayetano Istepeque', 10),
(165, 'San Esteban Catarina ', 10),
(166, 'San Ildefonso ', 10),
(167, 'San Lorenzo', 10),
(168, 'San Sebastián', 10),
(169, 'San Vicente', 10),
(170, 'Santa Clara', 10),
(171, 'Santo Domingo', 10),
(172, 'Tecoluca', 10),
(173, 'Tepetitán ', 10),
(174, 'Verapaz ', 10),
(175, 'Alegría', 11),
(176, 'Berlín', 11),
(177, 'California', 11),
(178, 'Concepción Batres', 11),
(179, 'El Triunfo', 11),
(180, 'Ereguayquín', 11),
(181, 'Estanzuelas', 11),
(182, 'Jiquilisco', 11),
(183, 'Jucuapa', 11),
(184, 'Jucuarán', 11),
(185, 'Mercedes Umaña', 11),
(186, 'Nueva Granada', 11),
(187, 'Ozatlán', 11),
(188, 'Puerto El Triunfo', 11),
(189, 'San Agustín', 11),
(190, 'San Buenaventura', 11),
(191, 'San Dionisio', 11),
(192, 'San Francisco Javier', 11),
(193, 'Santa Elena', 11),
(194, 'Santa María', 11),
(195, 'Santiago de María', 11),
(196, 'Tecapán', 11),
(197, 'Usulután', 11),
(198, 'Carolina', 12),
(199, 'Chapeltique', 12),
(200, 'Chinameca', 12),
(201, 'Chirilagua', 12),
(202, 'Ciudad Barrios', 12),
(203, 'Comacarán', 12),
(204, 'El Tránsito', 12),
(205, 'Lolotique', 12),
(206, 'Moncagua', 12),
(207, 'Nueva Guadalupe', 12),
(208, 'Nuevo Edén de San Juan', 12),
(209, 'Quelepa', 12),
(210, 'San Antonio del Mosco', 12),
(211, 'San Gerardo', 12),
(212, 'San Jorge', 12),
(213, 'San Luis de la Reina', 12),
(214, 'San Miguel', 12),
(215, 'San Rafael Oriente', 12),
(216, 'Sesori', 12),
(217, 'Uluazapa', 12),
(218, 'Arambala', 13),
(219, 'Cacaopera ', 13),
(220, 'Chilanga', 13),
(221, 'Corinto', 13),
(222, 'Delicias de Concepción', 13),
(223, 'El Divisadero', 13),
(224, 'El Rosario', 13),
(225, 'Gualococti', 13),
(226, 'Guatajiagua', 13),
(227, 'Joateca', 13),
(228, 'Jocoaitique', 13),
(229, 'Jocoro', 13),
(230, 'Lolotiquillo', 13),
(231, 'Meanguera', 13),
(232, 'Osicala', 13),
(233, 'Perquín', 13),
(234, 'San Carlos', 13),
(235, 'San Fernando', 13),
(236, 'San Francisco Gotera', 13),
(237, 'San Isidro', 13),
(238, 'San Simón', 13),
(239, 'Sensembra', 13),
(240, 'Sociedad', 13),
(241, 'Torola', 13),
(242, 'Yamabal', 13),
(243, 'Yoloaiquín', 13),
(244, 'La Unión', 14),
(245, 'San Alejo', 14),
(246, 'Yucuaiquín', 14),
(247, 'Conchagua', 14),
(248, 'Intipucá', 14),
(249, 'San José', 14),
(250, 'El Carmen', 14),
(251, 'Yayantique', 14),
(252, 'Bolívar', 14),
(253, 'Meanguera del Golfo', 14),
(254, 'Santa Rosa de Lima', 14),
(255, 'Pasaquina', 14),
(256, 'Anamorós', 14),
(257, 'Nueva Esparta', 14),
(258, 'El Sauce', 14),
(259, 'Concepción de Oriente', 14),
(260, 'Polorós', 14),
(261, 'Lislique', 14),
(262, 'Tonacatepeque', 14),
(263, 'No proporcionado', 15);

-- --------------------------------------------------------

--
-- Table structure for table `perfil`
--

CREATE TABLE `perfil` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perfil`
--

INSERT INTO `perfil` (`id`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Editor');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `usuario` varchar(400) DEFAULT NULL,
  `clave` text,
  `nombres` varchar(300) NOT NULL,
  `apellidos` varchar(300) NOT NULL,
  `perfil` int(11) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `clave`, `nombres`, `apellidos`, `perfil`, `fecha_registro`) VALUES
(1, 'Administrador', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Steve', 'Jhonson', 1, '2018-11-09 17:26:00'),
(4, 'ulises', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Ulises', 'Guzman', 2, '2018-11-29 23:22:32');

-- --------------------------------------------------------

--
-- Table structure for table `vivienda`
--

CREATE TABLE `vivienda` (
  `id` int(11) NOT NULL,
  `titulo` varchar(400) NOT NULL,
  `precio` varchar(300) DEFAULT NULL,
  `id_municipio` int(11) DEFAULT NULL,
  `direccion` text,
  `area_total` int(11) DEFAULT NULL,
  `area_construida` int(11) DEFAULT NULL,
  `cantidad_cuartos` int(11) DEFAULT NULL,
  `tipo_piso` varchar(100) DEFAULT NULL,
  `energia_electrica` tinyint(4) DEFAULT NULL,
  `agua` tinyint(4) DEFAULT NULL,
  `tipo_techo` varchar(100) DEFAULT NULL,
  `cantidad_banios` int(11) DEFAULT NULL,
  `terraza` tinyint(4) DEFAULT NULL,
  `cocina_amueblada` tinyint(4) DEFAULT NULL,
  `cochera` int(11) DEFAULT NULL,
  `jardin` int(11) DEFAULT NULL,
  `estado` varchar(100) NOT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `is_negociable` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vivienda`
--

INSERT INTO `vivienda` (`id`, `titulo`, `precio`, `id_municipio`, `direccion`, `area_total`, `area_construida`, `cantidad_cuartos`, `tipo_piso`, `energia_electrica`, `agua`, `tipo_techo`, `cantidad_banios`, `terraza`, `cocina_amueblada`, `cochera`, `jardin`, `estado`, `id_categoria`, `is_featured`, `is_negociable`) VALUES
(9, 'Casa en Chalatenango', '50000.00', 36, 'Linda Casa en Dulce Nombre de Maria, las mejores vistas en esta casa de campo.', 15000, 14000, 4, 'Cerámica', 1, 1, 'Cerámica', 2, 0, 0, 1, 1, 'Usada', 2, 1, 1),
(5, 'Casa en Santa Ana', '122333.00', 1, '6703 NW 7th Street', 31333, 4444, 2, 'Cerámica', 1, 1, 'Teja', 1, 0, 1, 1, 1, 'Nueva', 2, 1, 1),
(6, 'test home', '2323.00', 31, '6703 NW 7th Street', 2344, 23, 2, 'Cerámica', 1, 1, 'Cerámica', 1, 0, 0, 1, 1, 'Nueva', 1, 1, 0),
(7, 'Casa en Ilobasco', '4333.00', 156, 'Ilobasco Street Calle XYZ', 1500, 1400, 1, 'Cerámica', 1, 1, 'Cerámica', 1, 0, 1, 1, 1, 'Usada', 2, 0, 0),
(8, 'Casa en Sonsonate', '1200.00', 21, 'Barrio Santiago Armenia', 1500, 1400, 1, 'Cerámica', 1, 1, 'Cerámica', 1, 0, 0, 0, 1, 'Usada', 2, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vivienda`
--
ALTER TABLE `vivienda`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_municipio` (`id_municipio`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `departamento`
--
ALTER TABLE `departamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `fotos`
--
ALTER TABLE `fotos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `municipio`
--
ALTER TABLE `municipio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=264;

--
-- AUTO_INCREMENT for table `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vivienda`
--
ALTER TABLE `vivienda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
