<!DOCTYPE html>
<html lang="en">
<head>
<title>Vencore</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Vencore mi espacio, compra y venta de bienes raíces, encuentra la vivienda de tus sueños">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/about_styles.css">
<link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
</head>

<body>

<div class="super_container">
	
	<!-- Home -->
	<div class="home">
		<!-- Image by: https://unsplash.com/@jbriscoe -->
		<div class="home_background" style="background-image:url(images/home_background.jpg)"></div>
	</div>

	<!-- Header -->

	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">

						<!-- Logo -->

						<div class="logo_container">
							<a href="#">
								<div class="logo">
									<img src="images/logovencore.png" alt="" width="55%">
								</div>
							</a>
						</div>
						
						<!-- Main Navigation -->

						<nav class="main_nav">
							<ul class="main_nav_list">
								<li class="main_nav_item"><a href="index.php">Home</a></li>
								<li class="main_nav_item"><a href="listings.php">Viviendas</a></li>
								<li class="main_nav_item"><a href="legals.php">Asesoría legal</a></li>
								<li class="main_nav_item"><a href="contact.php">Contacto</a></li>
							</ul>
						</nav>
						
						<!-- Phone Home -->

						<div class="phone_home text-center">
							<span><a href="tel:50325104022" style="color:#FFFFFF;">Llame al +50325104022</a></span>
						</div>
						
						<!-- Hamburger -->

						<div class="hamburger_container menu_mm">
							<div class="hamburger menu_mm">
								<i class="fas fa-bars trans_200 menu_mm"></i>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->

			
		<div class="menu menu_mm">
			<ul class="menu_list">
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="index.php">home</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="listings.php">Viviendas</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="legals.php">Asesoría legal</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="contact.php">Contacto</a>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

	</header>

	<!-- Intro -->

	<div class="intro">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 order-lg-1 order-2">
					<h2 class="intro_title">Servicios jurídicos diversos.</h2>
					<br/>
					<div class="intro_subtitle">Licenciada Alicia Concepción Solís Rivera</div>
					<div class="intro_subtitle">Abogada y notario</div>
					<div class="intro_subtitle">Móvil: 0019518923061</div>
					<div class="intro_subtitle">E-mail: bufetesolisrivera@gmail.com, alices_77@hotmail.com</div>
					<br/>
					<p class="intro_text">Servicios disponibles: poderes o carta poder, autorización de salida de menores, autorización para tramitar pasaporte a menores de edad, aceptación de herencia, divorcios, diligencias por errores en certificados de nacimiento, compraventas de inmuebles, vehículos, todos estos documentos para hacerse en El Salvador,  apostillas en El Salvador y California. Asesorías de todo trámite legal en El Salvador.</p>
<br/>
<p class="intro_text">También cuento con servicios en el extranjero en las siguientes ciudades: Baja California, Tijuana,  Mexico, San Diego, Riverside, Corona, San Bernandino, Ontario, Pasadena, Los Ángeles, Valle San Fernando, Arleta, Pacoima, North Hollywood y todas las ciudades aledañas a estás, servicio a domicilio disponible.</p>
				 
				</div>
				<div class="col-lg-5 order-lg-2 order-1">
					<div class="intro_image">
						<img src="images/legal.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
 
 
	<!-- Footer -->

<footer class="footer">
		<div class="container">
			<div class="row">
				
				<!-- Footer About -->

				<div class="col-lg-3 footer_col">
					<div class="footer_social">
						<ul class="footer_social_list"> 
							<li class="footer_social_item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-twitter"></i></a></li>
						</ul>
					</div>
					 <div class="footer_about">
						<p>Vencore es una empresa salvadoreña con amplia trayectoria en la asesoría de compra y venta de bienes raíces, contamos con una base amplia de clientes que dan fe de nuestro profesionalismo y trabajo dedicado 100% a la satisfacción del servicio que ofrecemos.</p>
					</div>
					 
				</div>
				
				<!-- Footer Useful Links -->

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">Enlaces útiles</div>
					<ul class="footer_useful_links">
						<li class="useful_links_item"><a href="index.php">Home</a></li>
						<li class="useful_links_item"><a href="listings.php">Viviendas</a></li>
						<li class="useful_links_item"><a href="legals.php">Asesoría legal</a></li>
						<li class="useful_links_item"><a href="contact.php">Contacto</a></li>
					</ul>
				</div>

				<!-- Footer Contact Form -->
				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">Escribanos</div>
					<div class="footer_contact_form_container">
						<form id="footer_contact_form" class="footer_contact_form" action="post">
							<input id="contact_form_name" class="input_field contact_form_name" type="text" placeholder="Nombre" required="required" data-error="Name is required.">
							<input id="contact_form_email" class="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required.">
							<textarea id="contact_form_message" class="text_field contact_form_message" name="message" placeholder="Mensaje" required="required" data-error="Please, write us a message."></textarea>
							<button id="contact_send_btn" type="submit" class="contact_send_btn trans_200" value="Submit">Enviar</button>
						</form>
					</div>
				</div>

				<!-- Footer Contact Info -->

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">Información</div>
					<ul class="contact_info_list">
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/placeholder.svg" alt=""></div></div>
							<div class="contact_info_text">8a Avenida Nte No.3-8 Santa Tecla. La Libertad</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/phone-call.svg" alt=""></div></div>
							<div class="contact_info_text">(503) 2510 4022</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/message.svg" alt=""></div></div>
							<div class="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">ventas@vencoremiespacio.com</a></div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/planet-earth.svg" alt=""></div></div>
							<div class="contact_info_text"><a>WhatsApp +50377473063</a></div>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</footer>

	<!-- Credits -->

	<div class="credits">
		<span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Este portal ha sido desarrollado <i class="fa fa-heart" aria-hidden="true"></i> por <a href="https://adisingenieros.com" target="_blank">ADIS Ingenieros</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
	</div>

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/scrollTo/jquery.scrollTo.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/about_custom.js"></script>
</body>

</html>