<!DOCTYPE html>
<html lang="en">
<head>
<title>Vencore</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Vencore mi espacio, compra y venta de bienes raíces, encuentra la vivienda de tus sueños">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/contact_styles.css">
<link rel="stylesheet" type="text/css" href="styles/contact_responsive.css">
</head>

<body>

<div class="super_container">
	
	<!-- Home -->
	<div class="home">
		<div class="home_background" style="background-image:url(images/contact.jpg)"></div>
	</div>

	<!-- Header -->

	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">

						<!-- Logo -->

						<div class="logo_container">
							<a href="#">
								<div class="logo">
									<img src="images/logovencore.png" alt="" width="55%">
								</div>
							</a>
						</div>
						
						<!-- Main Navigation -->

						<nav class="main_nav">
							<ul class="main_nav_list">
									<li class="main_nav_item"><a href="index.php">Home</a></li>
								<li class="main_nav_item"><a href="listings.php">Viviendas</a></li>
								<li class="main_nav_item"><a href="legals.php">Asesoría legal</a></li>
								<li class="main_nav_item"><a href="contact.php">Contacto</a></li>
							</ul>
						</nav>
						
						<!-- Phone Home -->

						<div class="phone_home text-center">
							<span><a href="tel:50325104022" style="color:#FFFFFF;">Llame al +50325104022</a></span>
						</div>
						
						<!-- Hamburger -->

						<div class="hamburger_container menu_mm">
							<div class="hamburger menu_mm">
								<i class="fas fa-bars trans_200 menu_mm"></i>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->

		
		<div class="menu menu_mm">
			<ul class="menu_list">
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="index.php">home</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="listings.php">Viviendas</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="legals.php">Asesoría legal</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="contact.php">Contacto</a>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

	</header>

	<!-- Contact -->

	<div class="contact">
		<div class="container">
			<div class="row">
				
				<div class="col-lg-6 contact_col">
					<div class="estate_contact_form">
						<div class="contact_title">Escríbanos</div>
						<div class="estate_contact_form_container">
							<form id="estate_contact_form" class="estate_contact_form" action="post">
								<input id="estate_contact_form_name" class="estate_input_field estate_contact_form_name" type="text" placeholder="Nombre" required="required" data-error="Name is required.">
								<input id="estate_contact_form_email" class="estate_input_field estate_contact_form_email" type="email" placeholder="Correo electrónico" required="required" data-error="Valid email is required.">
								<input id="estate_contact_form_subject" class="estate_input_field estate_contact_form_subject" type="email" placeholder="Motivo" required="required" data-error="Subject is required.">
								<textarea id="estate_contact_form_message" class="estate_text_field estate_contact_form_message" name="message" placeholder="Mensaje" required="required" data-error="Please, write us a message."></textarea>
								<button id="estate_contact_send_btn" type="submit" class="estate_contact_send_btn trans_200" value="Submit">Enviar</button>
							</form>
						</div>
					</div>
				</div>

				<div class="col-lg-3 contact_col">
					<div class="contact_title">Contáctenos</div>
					<ul class="contact_info_list estate_contact">
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/placeholder.svg" alt=""></div></div>
							<div class="contact_info_text">8a Avenida Nte No.3-8 Santa Tecla. La Libertad, El Salvador C.A</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/phone-call.svg" alt=""></div></div>
							<div class="contact_info_text">(503) 2510 4022</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/message.svg" alt=""></div></div>
							<div class="contact_info_text"><a href="mailto:ventas@vencoremiespacio.com?Subject=Hello" target="_top">ventas@vencoremiespacio.com</a></div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/planet-earth.svg" alt=""></div></div>
							<div class="contact_info_text"><a>WhatsApp 50377473063</a></div>
						</li>
					</ul>
					<div class="estate_social">
						<ul class="estate_social_list">
						<!-- 	<li class="estate_social_item"><a href="#"><i class="fab fa-pinterest"></i></a></li> -->
							<li class="estate_social_item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li class="estate_social_item"><a href="#"><i class="fab fa-twitter"></i></a></li>
						</ul>
					</div>
				</div>
 

			</div>

		</div>

	<!-- Footer -->
	<footer class="footer">
		<div class="container">
			<div class="row">
				
				<!-- Footer About -->

				<div class="col-lg-3 footer_col">
					<div class="footer_social">
						<ul class="footer_social_list"> 
							<li class="footer_social_item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-twitter"></i></a></li>
						</ul>
					</div>
					 <div class="footer_about">
						<p>Vencore es una empresa salvadoreña con amplia trayectoria en la asesoría de compra y venta de bienes raíces, contamos con una base amplia de clientes que dan fe de nuestro profesionalismo y trabajo dedicado 100% a la satisfacción del servicio que ofrecemos.</p>
					</div>
					 
				</div>
				
				<!-- Footer Useful Links -->

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">Enlaces útiles</div>
					<ul class="footer_useful_links">
						<li class="useful_links_item"><a href="index.php">Home</a></li>
						<li class="useful_links_item"><a href="listings.php">Viviendas</a></li>
						<li class="useful_links_item"><a href="legals.php">Asesoría legal</a></li>
						<li class="useful_links_item"><a href="contact.php">Contacto</a></li>
					</ul>
				</div>

				<!-- Footer Contact Form -->
				<!-- <div class="col-lg-3 footer_col">
					<div class="footer_col_title">Escribanos</div>
					<div class="footer_contact_form_container">
						<form id="footer_contact_form" class="footer_contact_form" action="post">
							<input id="contact_form_name" class="input_field contact_form_name" type="text" placeholder="Nombre" required="required" data-error="Name is required.">
							<input id="contact_form_email" class="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required.">
							<textarea id="contact_form_message" class="text_field contact_form_message" name="message" placeholder="Mensaje" required="required" data-error="Please, write us a message."></textarea>
							<button id="contact_send_btn" type="submit" class="contact_send_btn trans_200" value="Submit">Enviar</button>
						</form>
					</div>
				</div> -->

				<!-- Footer Contact Info -->

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">Información</div>
					<ul class="contact_info_list">
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/placeholder.svg" alt=""></div></div>
							<div class="contact_info_text">8a Avenida Nte No.3-8 Santa Tecla. La Libertad</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/phone-call.svg" alt=""></div></div>
							<div class="contact_info_text">(503) 2510 4022</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/message.svg" alt=""></div></div>
							<div class="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">ventas@vencoremiespacio.com</a></div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/planet-earth.svg" alt=""></div></div>
							<div class="contact_info_text"><a>WhatsApp +50377473063</a></div>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</footer>

	<!-- Credits -->

	<div class="credits">
		<span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Este portal ha sido desarrollado <i class="fa fa-heart" aria-hidden="true"></i> por <a href="https://adisingenieros.com" target="_blank">ADIS Ingenieros</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
	</div>

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/contact_custom.js"></script>
</body>

</html>