<!DOCTYPE html>
<html lang="en">
<head>
<title>Vencore</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Vencore mi espacio, compra y venta de bienes raíces, encuentra la vivienda de tus sueños">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/listings_styles.css?id=2">
<link rel="stylesheet" type="text/css" href="styles/listings_responsive.css">	
</head>

<body>

<div class="super_container">
	<!-- Header -->
	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">

						<!-- Logo -->

						<div class="logo_container">
							<a href="#">
								<div class="logo">
									<img src="images/logovencore.png" alt="" width="55%">
								</div>
							</a>
						</div>
						
						<!-- Main Navigation -->

						<nav class="main_nav">
							<ul class="main_nav_list">
								<li class="main_nav_item"><a href="index.php">Home</a></li>
								<li class="main_nav_item"><a href="listings.php">Viviendas</a></li>
								<li class="main_nav_item"><a href="legals.php">Asesoría legal</a></li>
								<li class="main_nav_item"><a href="contact.php">Contacto</a></li>
							</ul>
						</nav>
						
						<!-- Phone Home -->

						<div class="phone_home text-center">
							<span><a href="tel:50325104022" style="color:#FFFFFF;">Llame al +50325104022</a></span>
						</div>
						
						<!-- Hamburger -->

						<div class="hamburger_container menu_mm">
							<div class="hamburger menu_mm">
								<i class="fas fa-bars trans_200 menu_mm"></i>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->

			
		<div class="menu menu_mm">
			<ul class="menu_list">
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="index.php">home</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="listings.php">Viviendas</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="legals.php">Asesoría legal</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="contact.php">Contacto</a>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

	</header>

	<!-- Home -->
	<div class="home">
		<!-- Image by: https://unsplash.com/@jbriscoe -->
		<div class="home_background" style="background-image:url(images/listings.jpg)"></div>
	</div>
	<!-- Listings -->

	<div class="listings" ng-app="vencore" ng-controller="housesController">
		<div class="container">
			<div class="row">
				
				<!-- Search Sidebar -->

				<div class="col-lg-4 sidebar_col">
					<!-- Search Box -->

					<div class="search_box">

						<div class="search_box_content">

							<!-- Search Box Title -->
							<div class="search_box_title text-center">
								<div class="search_box_title_inner">
									<div class="search_box_title_icon d-flex flex-column align-items-center justify-content-center"><img src="images/search.png" alt=""></div>
									<span>Busque su hogar</span>
								</div>
							</div>

							<!-- Search Form -->
							<form class="search_form" action="#">
								<div class="search_box_container">
									<ul class="dropdown_row clearfix">
										 <li>
							               <label class="radio-inline"><input type="radio" id="estado" name="estado" value="Nueva" checked="checked"> Nueva</label>
							               <label class="radio-inline"><input type="radio" id="estado" name="estado" value="Usada"> Usada</label>
										</li>
										<li class="dropdown_item dropdown_item_5">
											<div class="dropdown_item_title">Municipio</div>
											<select name="id_municipio" id="id_municipio" class="dropdown_item_select" ng-controller="municipiosController">
												<option value="0">Cualquiera</option>
												<option ng-repeat="municipio in municipios" value="{{municipio.id}}">{{municipio.nombre}}</option>
											</select>
										</li>
									
										<li class="dropdown_item dropdown_item_6">
											<div class="dropdown_item_title">Tipo</div>
											<select name="tipo" id="tipo" class="dropdown_item_select">
												<option value="0">Cualquiera</option>
												<option value="1">Apartamento</option>
												<option value="2">Casa</option>
												<option value="3">Terreno</option>
												<option value="4">Finca</option>
												<option value="5">Rancho de Playa</option>
												<option value="6">Otro</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_6">
											<div class="dropdown_item_title">Precio mínimo</div>
											<input type="number" min="0.00"  step="0.01" name="precio_minimo" id="precio_minimo" class="input_item_home" value="0" />
										</li>
										<li class="dropdown_item dropdown_item_6">
											<div class="dropdown_item_title">Precio máximo</div>
											<input type="number" min="0.00" step="0.01" name="precio_maximo" id="precio_maximo" class="input_item_home" value="0" />
										</li>
										<li class="dropdown_item">
											<div class="search_button">
												<input value="search" type="button" name="searchButton" id="searchButton" class="search_submit_button" ng-click='search($event)'>
											</div>
										</li>	
									</ul>
								</div>
								
							</form>
						</div>	
					</div>
				</div>

				<!-- Listings -->

				<div class="col-lg-8 listings_col">

					<!-- Listings Item -->

					<div class="listing_item" ng-repeat="x in houses">
						<div class="listing_item_inner d-flex flex-md-row flex-column trans_300">
							<div class="listing_image_container">
								<div class="listing_image">
									<!-- Image by: https://unsplash.com/@breather -->
									<div class="listing_background" style="background-image:url({{API}}assets/pictures/{{x.id}}/{{x.fotos[0].url}})"></div>
								</div>
								<div class="featured_card_box d-flex flex-row align-items-center trans_300">
									<img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g">
									<div class="featured_card_box_content">
										<div class="featured_card_price_title trans_300">En venta - {{x.estado}}</div>
										<div class="featured_card_price trans_300">{{x.precio | currency}}</div>
									</div>
								</div>
							</div>
							<div class="listing_content">
								<div class="listing_title"><a href="listings_single.php?id={{x.id}}">{{x.id}} - {{x.titulo}}</a></div>
								<div class="listing_text">{{x.direccion}}</div>
								<div class="rooms">

									<div class="room">
										<span class="room_title">Habitaciones</span>
										<div class="room_content">
											<div class="room_image"><img src="images/bedroom.png" alt=""></div>
											<span class="room_number">{{x.cantidad_cuartos}}</span>
										</div>
									</div>

									<div class="room">
										<span class="room_title">Baños</span>
										<div class="room_content">
											<div class="room_image"><img src="images/shower.png" alt=""></div>
											<span class="room_number">{{x.cantidad_banios}}</span>
										</div>
									</div>

									<div class="room">
										<span class="room_title">Area total</span>
										<div class="room_content">
											<div class="room_image"><img src="images/area.png" alt=""></div>
											<span class="room_number">{{x.area_total | number}} m²</span>
										</div>
									</div>
									<div class="room">
										<span class="room_title">Area construída</span>
										<div class="room_content">
											<div class="room_image"><img src="images/area.png" alt=""></div>
											<span class="room_number">{{x.area_construida | number}} m²</span>
										</div>
									</div>
									<div class="room">
										<span class="room_title">Jardín</span>
										<div class="room_content">
											<div class="room_image"><img src="images/patio.png" alt=""></div>
											<span class="room_number">{{x.jardin}}</span>
										</div>
									</div>

									<div class="room">
										<span class="room_title">Cochera</span>
										<div class="room_content">
											<div class="room_image"><img src="images/garage.png" alt=""></div>
											<span class="room_number">{{x.cochera}}</span>
										</div>
									</div>

								</div>

								<div class="room_tags">
									<span class="room_tag" ng-if="x.terraza"><a href="#">Terraza</a></span>
									<span class="room_tag" ng-if="x.cocina_amueblada"><a href="#">Cocina amueblada</a></span>
									<span class="room_tag" ng-if="x.jardin"><a href="#">Jardín</a></span>
									<span class="room_tag" ng-if="x.energia_electrica"><a href="#">Energía Eléctrica</a></span>
									<span class="room_tag" ng-if="x.agua"><a href="#">Agua potable</a></span>
								</div>
							</div>
						</div>
					</div>

					 
					<!-- Finish listing item -->
				</div>

			</div>

			<div class="row">
				<div class="col clearfix">
					<div class="listings_nav"> 
						<ul>
							 <li class="listings_nav_item" ng-repeat="page in pages">
								<a ng-click='selectedPage($event)'>{{$index+1}}</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Newsletter -->

<!-- 	<div class="newsletter">
		<div class="container">
			<div class="row row-equal-height">

				<div class="col-lg-6">
					<div class="newsletter_title">
						<h3>subscribe to our newsletter</h3>
						<span class="newsletter_subtitle">Get the latest offers</span>
					</div>
					<div class="newsletter_form_container">
						<form action="#">
							<div class="newsletter_form_content d-flex flex-row">
								<input id="newsletter_email" class="newsletter_email" type="email" placeholder="Your email here" required="required" data-error="Valid email is required.">
								<button id="newsletter_submit" type="submit" class="newsletter_submit_btn trans_200" value="Submit">subscribe</button>
							</div>
						</form>
					</div>
				</div>

				<div class="col-lg-6">
					<a href="#">
						<div class="weekly_offer">
							<div class="weekly_offer_content d-flex flex-row">
								<div class="weekly_offer_icon d-flex flex-column align-items-center justify-content-center">
									<img src="images/prize.svg" alt="">
								</div>
								<div class="weekly_offer_text text-center">weekly offer</div>
							</div>
							<div class="weekly_offer_image" style="background-image:url(images/weekly.jpg)"></div>
						</div>
					</a>
				</div>

			</div>
		</div>
	</div> -->

	<!-- Footer -->

<footer class="footer">
		<div class="container">
			<div class="row">
				
				<!-- Footer About -->

				<div class="col-lg-3 footer_col">
					<div class="footer_social">
						<ul class="footer_social_list"> 
							<li class="footer_social_item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-twitter"></i></a></li>
						</ul>
					</div>
					 <div class="footer_about">
						<p>Vencore es una empresa salvadoreña con amplia trayectoria en la asesoría de compra y venta de bienes raíces, contamos con una base amplia de clientes que dan fe de nuestro profesionalismo y trabajo dedicado 100% a la satisfacción del servicio que ofrecemos.</p>
					</div>
					 
				</div>
				
				<!-- Footer Useful Links -->

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">Enlaces útiles</div>
					<ul class="footer_useful_links">
						<li class="useful_links_item"><a href="index.php">Home</a></li>
						<li class="useful_links_item"><a href="listings.php">Viviendas</a></li>
						<li class="useful_links_item"><a href="legals.php">Asesoría legal</a></li>
						<li class="useful_links_item"><a href="contact.php">Contacto</a></li>
					</ul>
				</div>

				<!-- Footer Contact Form -->
				<!-- <div class="col-lg-3 footer_col">
					<div class="footer_col_title">Escribanos</div>
					<div class="footer_contact_form_container">
						<form id="footer_contact_form" class="footer_contact_form" action="post">
							<input id="contact_form_name" class="input_field contact_form_name" type="text" placeholder="Nombre" required="required" data-error="Name is required.">
							<input id="contact_form_email" class="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required.">
							<textarea id="contact_form_message" class="text_field contact_form_message" name="message" placeholder="Mensaje" required="required" data-error="Please, write us a message."></textarea>
							<button id="contact_send_btn" type="submit" class="contact_send_btn trans_200" value="Submit">Enviar</button>
						</form>
					</div>
				</div> -->

				<!-- Footer Contact Info -->

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">Información</div>
					<ul class="contact_info_list">
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/placeholder.svg" alt=""></div></div>
							<div class="contact_info_text">8a Avenida Nte No.3-8 Santa Tecla. La Libertad</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/phone-call.svg" alt=""></div></div>
							<div class="contact_info_text">(503) 2510 4022</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/message.svg" alt=""></div></div>
							<div class="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">ventas@vencoremiespacio.com</a></div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/planet-earth.svg" alt=""></div></div>
							<div class="contact_info_text"><a>WhatsApp +50377473063</a></div>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</footer>

	<!-- Credits -->

	<div class="credits">
		<span>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Este portal ha sido desarrollado <i class="fa fa-heart" aria-hidden="true"></i> por <a href="https://adisingenieros.com" target="_blank">ADIS Ingenieros</a>
</span>
	</div>

</div>

<script src="js/main.js"></script>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/angular.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/listings_custom.js"></script>
<script src="js/vencore.js?id=4"></script>	
</body>

</html>