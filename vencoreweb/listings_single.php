<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Vencore</title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="description" content="Vencore mi espacio, compra y venta de bienes raíces, encuentra la vivienda de tus sueños">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
      <link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="bower_components/slick-carousel/slick/slick.css">
      <link rel="stylesheet" href="bower_components/slick-carousel/slick/slick-theme.css">
      <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="styles/listings_single_styles.css?id=2">
      <link rel="stylesheet" type="text/css" href="styles/listings_single_responsive.css">
      <style type="text/css">
         #slick{
            width: 60%;
            display: inline-block !important;
         }
         .listing_slider_container{
            text-align: center;
         }

         .slick-prev:before, .slick-next:before { 
            color:#000 !important;
         }

         @media (min-width: 481px) and (max-width: 767px) {
            #slick{
               width: 80%;
               display: inline-block !important;
            }   
         }

         /* 
           ##Device = Most of the Smartphones Mobiles (Portrait)
           ##Screen = B/w 320px to 479px
         */

         @media (min-width: 320px) and (max-width: 480px) {
             #slick{
               width: 90%;
               display: inline-block !important;
            }   
         }
         
      </style>
   </head>
   <body>
      <div class="super_container" ng-app="vencore2" ng-controller="detailController">
         <!-- Home -->
         <div class="home">
            <!-- Image by: https://unsplash.com/@jbriscoe -->
            <div class="home_background" style="background-image:url(images/listings_single.jpg)"></div>
            <div class="container">
               <div class="row">
               </div>
            </div>
         </div>
         <!-- Header -->
         <header class="header trans_300">
            <div class="container">
               <div class="row">
                  <div class="col">
                     <div class="header_container d-flex flex-row align-items-center trans_300">
                        <!-- Logo -->
                        <div class="logo_container">
                           <a href="#">
                              <div class="logo">
                                 <img src="images/logovencore.png" alt="">
                              </div>
                           </a>
                        </div>
                        <!-- Main Navigation -->
                        <nav class="main_nav">
                           <ul class="main_nav_list">
                              <li class="main_nav_item"><a href="index.php">Home</a></li>
                              <li class="main_nav_item"><a href="listings.php">Viviendas</a></li>
                              <li class="main_nav_item"><a href="legals.php">Asesoría legal</a></li>
                              <li class="main_nav_item"><a href="contact.php">Contacto</a></li>
                           </ul>
                        </nav>
                        <!-- Phone Home -->
                        <div class="phone_home text-center">
                           <span><a href="tel:50325104022" style="color:#FFFFFF;">Llame al +50325104022</a></span>
                        </div>
                        <!-- Hamburger -->
                        <div class="hamburger_container menu_mm">
                           <div class="hamburger menu_mm">
                              <i class="fas fa-bars trans_200 menu_mm"></i>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Menu -->
            <div class="menu menu_mm">
               <ul class="menu_list">
                  <li class="menu_item">
                     <div class="container">
                        <div class="row">
                           <div class="col">
                              <a href="index.php">home</a>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="menu_item">
                     <div class="container">
                        <div class="row">
                           <div class="col">
                              <a href="listings.php">Viviendas</a>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="menu_item">
                     <div class="container">
                        <div class="row">
                           <div class="col">
                              <a href="legals.php">Asesoría legal</a>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="menu_item">
                     <div class="container">
                        <div class="row">
                           <div class="col">
                              <a href="contact.php">Contacto</a>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
            </div>
         </header>
         <!-- Listing -->
         <div class="listing">
            <div class="container">
               <div class="row">
                  <div class="col-lg-8">
                     <!-- Listing Title -->
                     <div class="listing_title_container">
                        <div class="listing_title">{{house[0].id}} - {{house[0].titulo}}</div>
                        <p class="listing_text">{{hose[0].direccion}}</p>
                        <div class="room_tags">
                           <span class="room_tag" ng-if="house[0].terraza"><a href="#">Terraza</a></span>
                           <span class="room_tag" ng-if="house[0].cocina_amueblada"><a href="#">Cocina amueblada</a></span>
                           <span class="room_tag" ng-if="house[0].jardin"><a href="#">Jardín</a></span>
                           <span class="room_tag" ng-if="house[0].energia_electrica"><a href="#">Energía Eléctrica</a></span>
                           <span class="room_tag" ng-if="house[0].agua"><a href="#">Agua potable</a></span>
                        </div>
                     </div>
                  </div>
                  <!-- Listing Price -->
                  <div class="col-lg-4 listing_price_col clearfix">
                     <div class="featured_card_box d-flex flex-row align-items-center trans_300 float-lg-right">
                        <img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g">
                        <div class="featured_card_box_content">
                           <div class="featured_card_price_title trans_300" ng-if="house[0].is_negociable == 1">Negociable</div>
                           <div class="featured_card_price_title trans_300" ng-if="house[0].is_negociable == 0">No Negociable</div>
                           <div class="featured_card_price trans_300">{{house[0].precio | currency}}</div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col">
                     <!-- Listing Image Slider -->
                     <div class="listing_slider_container">
                        <slick infinite=true settings="slickConfig" init-onload=true data="house" ng-if="house.length" id="slick">
                           <!--  ng if es importante para cargar la data en el callback y funcione el slick -->
                           <div ng-repeat="foto in house[0].fotos" id="aniimated-thumbnials">
                           <a href="{{API}}assets/pictures/{{foto.id_vivienda}}/{{foto.url}}" target="_blank">
                           <img id="lightgallery" 
                              ng-src="{{API}}assets/pictures/{{foto.id_vivienda}}/{{foto.url}}"
                              width="100%" />
                           </a>
                        </div>
                        </slick>   
                     </div>
                  </div>
               </div>
               <div class="row listing_content_row">
                  <!-- Search Sidebar -->
                  <!-- Listing -->
                  <div class="listing_col">
                     <div class="listing_details">
                        <div class="listing_subtitle">Facilidades de pago</div>
                        <p class="listing_details_text">Vencore te ofrece las mejores opciones de pago con nuestra red de bancos y cooperativas que estarán dispuestas a financiar tus sueños.</p>
                        <div class="rooms">
                           <div class="room">
                              <span class="room_title">Habitaciones</span>
                              <div class="room_content">
                                 <div class="room_image"><img src="images/bedroom.png" alt=""></div>
                                 <span class="room_number">4</span>
                              </div>
                           </div>
                           <div class="room">
                              <span class="room_title">Baños</span>
                              <div class="room_content">
                                 <div class="room_image"><img src="images/shower.png" alt=""></div>
                                 <span class="room_number">3</span>
                              </div>
                           </div>
                           <div class="room">
                              <span class="room_title">Area total</span>
                              <div class="room_content">
                                 <div class="room_image"><img src="images/area.png" alt=""></div>
                                 <span class="room_number">{{house[0].area_total | number}} mt</span>
                              </div>
                           </div>
                           <div class="room">
                              <span class="room_title">Area construída</span>
                              <div class="room_content">
                                 <div class="room_image"><img src="images/area.png" alt=""></div>
                                 <span class="room_number">{{house[0].area_construida | number}} mt</span>
                              </div>
                           </div>
                           <div class="room">
                              <span class="room_title">Jardínes</span>
                              <div class="room_content">
                                 <div class="room_image"><img src="images/patio.png" alt=""></div>
                                 <span class="room_number">{{house[0].jardin}}</span>
                              </div>
                           </div>
                           <div class="room">
                              <span class="room_title">Cocheras</span>
                              <div class="room_content">
                                 <div class="room_image"><img src="images/garage.png" alt=""></div>
                                 <span class="room_number">{{house[0].cochera}}</span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Listing Description -->
                     <div class="listing_description">
                        <div class="listing_subtitle">Descripción</div>
                        <p class="listing_description_text">{{house[0].direccion}}</p>
                     </div>
                     <!-- Listing Additional Details -->
                     <div class="listing_additional_details">
                        <div class="listing_subtitle">Detalles adicionales</div>
                        <ul class="additional_details_list">
                           <li class="additional_detail"><span>Estado de la vivienda: {{house[0].estado}}</span></li>
                           <li class="additional_detail"><span>Energía Eléctrica: </span>
                              <label ng-switch="house[0].energia_electrica">
                              <span ng-switch-when="1">Sí</span>
                              <span ng-switch-when="0">No</span>
                              </label>
                           </li>
                           <li class="additional_detail"><span>Agua potable: </span>
                              <label ng-switch="house[0].agua">
                              <span ng-switch-when="1">Sí</span>
                              <span ng-switch-when="0">No</span>
                              </label>
                           </li>
                           <li class="additional_detail"><span>Cocina amueblada: </span>
                              <label ng-switch="house[0].cocina_amueblada">
                              <span ng-switch-when="1">Sí</span>
                              <span ng-switch-when="0">No</span>
                              </label>
                           </li>
                           <li class="additional_detail"><span>Terraza: </span>
                              <label ng-switch="house[0].cocina_amueblada">
                              <span ng-switch-when="1">Sí</span>
                              <span ng-switch-when="0">No</span>
                              </label>
                           </li>
                           <li class="additional_detail"><span>Tipo de piso: {{house[0].tipo_piso}} </span></li>
                           <li class="additional_detail"><span>Tipo de techo: {{house[0].tipo_techo}} </span> </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Newsletter -->
         <!-- <div class="newsletter">
            <div class="container">
            	<div class="row row-equal-height">
            
            		<div class="col-lg-6">
            			<div class="newsletter_title">
            				<h3>subscribe to our newsletter</h3>
            				<span class="newsletter_subtitle">Get the latest offers</span>
            			</div>
            			<div class="newsletter_form_container">
            				<form action="#">
            					<div class="newsletter_form_content d-flex flex-row">
            						<input id="newsletter_email" class="newsletter_email" type="email" placeholder="Your email here" required="required" data-error="Valid email is required.">
            						<button id="newsletter_submit" type="submit" class="newsletter_submit_btn trans_200" value="Submit">subscribe</button>
            					</div>
            				</form>
            			</div>
            		</div>
            
            		<div class="col-lg-6">
            			<a href="#">
            				<div class="weekly_offer">
            					<div class="weekly_offer_content d-flex flex-row">
            						<div class="weekly_offer_icon d-flex flex-column align-items-center justify-content-center">
            							<img src="images/prize.svg" alt="">
            						</div>
            						<div class="weekly_offer_text text-center">weekly offer</div>
            					</div>
            					<div class="weekly_offer_image" style="background-image:url(images/weekly.jpg)"></div>
            				</div>
            			</a>
            		</div>
            
            	</div>
            </div>
            </div> -->
         <!-- Footer -->
         <footer class="footer">
            <div class="container">
               <div class="row">
                  <!-- Footer About -->
                  <div class="col-lg-3 footer_col">
                     <div class="footer_col_title">
                     </div>
                     <div class="footer_social">
                        <ul class="footer_social_list">
                           <li class="footer_social_item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                           <li class="footer_social_item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                        </ul>
                     </div>
                  </div>
                  <!-- Footer Useful Links -->
                  <div class="col-lg-3 footer_col">
                     <div class="footer_col_title">Enlaces útiles</div>
                     <ul class="footer_useful_links">
                        <li class="useful_links_item"><a href="index.php">Home</a></li>
                        <li class="useful_links_item"><a href="listings.php">Viviendas</a></li>
                        <li class="useful_links_item"><a href="legals.php">Asesoría legal</a></li>
                        <li class="useful_links_item"><a href="contact.php">Contacto</a></li>
                     </ul>
                  </div>
                  <!-- Footer Contact Form -->
                  <!-- <div class="col-lg-3 footer_col">
                     <div class="footer_col_title">Escribanos</div>
                     <div class="footer_contact_form_container">
                     	<form id="footer_contact_form" class="footer_contact_form" action="post">
                     		<input id="contact_form_name" class="input_field contact_form_name" type="text" placeholder="Nombre" required="required" data-error="Name is required.">
                     		<input id="contact_form_email" class="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required.">
                     		<textarea id="contact_form_message" class="text_field contact_form_message" name="message" placeholder="Mensaje" required="required" data-error="Please, write us a message."></textarea>
                     		<button id="contact_send_btn" type="submit" class="contact_send_btn trans_200" value="Submit">Enviar</button>
                     	</form>
                     </div>
                     </div> -->
                  <!-- Footer Contact Info -->
                  <div class="col-lg-3 footer_col">
                     <div class="footer_col_title">Información</div>
                     <ul class="contact_info_list">
                        <li class="contact_info_item d-flex flex-row">
                           <div>
                              <div class="contact_info_icon"><img src="images/placeholder.svg" alt=""></div>
                           </div>
                           <div class="contact_info_text">8a Avenida Nte No.3-8 Santa Tecla. La Libertad</div>
                        </li>
                        <li class="contact_info_item d-flex flex-row">
                           <div>
                              <div class="contact_info_icon"><img src="images/phone-call.svg" alt=""></div>
                           </div>
                           <div class="contact_info_text">(503) 2510 4022</div>
                        </li>
                        <li class="contact_info_item d-flex flex-row">
                           <div>
                              <div class="contact_info_icon"><img src="images/message.svg" alt=""></div>
                           </div>
                           <div class="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">ventas@vencoremiespacio.com</a></div>
                        </li>
                        <li class="contact_info_item d-flex flex-row">
                           <div>
                              <div class="contact_info_icon"><img src="images/planet-earth.svg" alt=""></div>
                           </div>
                           <div class="contact_info_text"><a>WhatsApp +50377473063</a></div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </footer>
         <!-- Credits -->
         <div class="credits">
            <span>
               Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Este portal ha sido desarrollado <i class="fa fa-heart" aria-hidden="true"></i> por <a href="https://adisingenieros.com" target="_blank">ADIS Ingenieros</a>
            </span>
         </div>
      </div>
      <script src="js/main.js?id=2323"></script>
      <script src="bower_components/jquery/dist/jquery.min.js"></script>	
      <script src="styles/bootstrap4/popper.js"></script>
      <script src="styles/bootstrap4/bootstrap.min.js"></script> 
      <script src="plugins/easing/easing.js"></script>
      <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
      <script src="js/listings_single_custom.js?id=67"></script>
      <script src="bower_components/angular/angular.js"></script>
      <script src="bower_components/slick-carousel/slick/slick.js"></script>
       <script src="js/vencore.js?id=56"></script>
      <script src="bower_components/angular-slick-carousel/dist/angular-slick.min.js"></script> 
   </body>
  
</html>