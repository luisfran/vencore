<!DOCTYPE html>
<html lang="en" ng-app="vencore">
<head>
<title>Vencore</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Vencore mi espacio, compra y venta de bienes raíces, encuentra la vivienda de tus sueños">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="styles/responsive.css">
</head>

<body>

<div class="super_container" ng-controller="featuredHousesController">
	
	<!-- Home -->
	<div class="home">
		
		<!-- Home Slider -->
		<div class="home_slider_container">
			<div class="owl-carousel owl-theme home_slider">

				<!-- Home Slider Item -->
				<div class="owl-item home_slider_item">
					<!-- Image by https://unsplash.com/@aahubs -->
					<div class="home_slider_background" style="background-image:url(images/home_slider_bcg.jpg)"></div>
					<div class="home_slider_content_container text-center">
						<div class="home_slider_content">
							<h1 data-animation-in="flipInX" data-animation-out="animate-out fadeOut">Vencore mi espacio</h1>
						</div>
					</div>
				</div>

				<!-- Home Slider Item -->
				<div class="owl-item home_slider_item">
					<!-- Image by https://unsplash.com/@aahubs -->
					<div class="home_slider_background" style="background-image:url(images/bg2.jpg)"></div>
					<div class="home_slider_content_container text-center">
						<div class="home_slider_content">
							<h1 data-animation-in="flipInX" data-animation-out="animate-out fadeOut">Vencore mi espacio</h1>
						</div>
					</div>
				</div>

				<!-- Home Slider Item -->
				<div class="owl-item home_slider_item">
					<!-- Image by https://unsplash.com/@aahubs -->
					<div class="home_slider_background" style="background-image:url(images/bg4.jpg)"></div>
					<div class="home_slider_content_container text-center">
						<div class="home_slider_content">
							<h1 data-animation-in="flipInX" data-animation-out="animate-out fadeOut">Vencore mi espacio</h1>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Home Slider Nav -->
			<div class="home_slider_nav_left home_slider_nav d-flex flex-row align-items-center justify-content-end">
				<img src="images/nav_left.png" alt="">
			</div>

		</div>

	</div>

	<!-- Header -->

	<header class="header trans_300">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">

						<!-- Logo -->

						<div class="logo_container">
							<a href="#">
								<div class="logo">
									<img src="images/logovencore.png" alt="">
									 
								</div>
							</a>
						</div>
						
						<!-- Main Navigation -->

						<nav class="main_nav">
							<ul class="main_nav_list">
								<li class="main_nav_item"><a href="index.php">Home</a></li>
								<li class="main_nav_item"><a href="listings.php">Viviendas</a></li>
								<li class="main_nav_item"><a href="legals.php">Asesoría legal</a></li>
								<li class="main_nav_item"><a href="contact.php">Contacto</a></li>
							</ul>
						</nav>
						
						<!-- Phone Home -->

						<div class="phone_home text-center">
							<span><a href="tel:50325104022" style="color:#FFFFFF;">Llame al +50325104022</a></span>
						</div>
						
						<!-- Hamburger -->

						<div class="hamburger_container menu_mm">
							<div class="hamburger menu_mm">
								<i class="fas fa-bars trans_200 menu_mm"></i>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->

		<div class="menu menu_mm">
			<ul class="menu_list">
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="index.php">home</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="listings.php">Viviendas</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="legals.php">Asesoría legal</a>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="contact.php">Contacto</a>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

	</header>
	
	<!-- Search Box -->

	<div class="search_box">
		<div class="container">
			<div class="row">
				<div class="col">

					<div class="search_box_outer">
						<div class="search_box_inner">

							<!-- Search Box Title -->
							<div class="search_box_title text-center">
								<div class="search_box_title_inner">
									<div class="search_box_title_icon d-flex flex-column align-items-center justify-content-center"><img src="images/search.png" alt=""></div>
									<span>Encuentra tu vivienda ideal</span>
								</div>
							</div>

							<!-- Search Arrow -->
							<div class="search_arrow_box">
								<div class="search_arrow_box_inner">
									<div class="search_arrow_circle d-flex flex-column align-items-center justify-content-center"><span>Buscar..</span></div>
									<img src="images/search_arrow.png" alt="">
								</div>
							</div>

							<!-- Search Form -->
							 <form class="search_form" action="#">
								<div class="search_box_container">
									<ul class="dropdown_row clearfix">
										<li>
							               <label class="radio-inline"><input type="radio" id="estado" name="estado" value="Nueva" checked="checked"> Nueva</label>
							               <label class="radio-inline"><input type="radio" id="estado" name="estado" value="Usada"> Usada</label>
										</li>
										<li class="dropdown_item dropdown_item_5">
											<div class="dropdown_item_title">Municipio</div>
											<select name="id_municipio" id="id_municipio" class="dropdown_item_select" ng-controller="municipiosController">
												<option value="0">Cualquiera</option>
												<option ng-repeat="municipio in municipios" value="{{municipio.id}}">{{municipio.nombre}}</option>
											</select>
										</li>
									
										<li class="dropdown_item dropdown_item_6">
											<div class="dropdown_item_title">Tipo</div>
											<select name="tipo" id="tipo" class="dropdown_item_select">
												<option value="0">Cualquiera</option>
												<option value="1">Apartamento</option>
												<option value="2">Casa</option>
												<option value="3">Terreno</option>
												<option value="4">Finca</option>
												<option value="5">Rancho de Playa</option>
												<option value="6">Otro</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_6">
											<div class="dropdown_item_title">Precio mínimo</div>
											<input type="number" min="0.00"  step="0.01" name="precio_minimo" id="precio_minimo" class="input_item_home" value="0" />
										</li>
										<li class="dropdown_item dropdown_item_6">
											<div class="dropdown_item_title">Precio máximo</div>
											<input type="number" min="0.00" step="0.01" name="precio_maximo" id="precio_maximo" class="input_item_home" value="0" />
										</li>
										<li class="dropdown_item">
											<div class="search_button">
												<input value="search" type="button" name="searchButton" id="searchButton" class="search_submit_button" ng-click='search($event)'>
											</div>
										</li>	
									</ul>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>		
	</div>

	<!-- Featured Properties -->

	<div class="featured" >
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h3>Encuentre las mejores viviendas en Vencore</h3>
						<span class="section_subtitle">Viviendas destacadas</span>
					</div>
				</div>
			</div>

			<div class="row featured_row">
				<div class="col-lg-4 featured_card_col" ng-repeat="x in houses">

					<div class="featured_card_container">
						<div class="card featured_card trans_300">
							<div class="featured_panel">{{x.estado}}</div>
							<img class="card-img-top" ng-src="{{API}}/assets/pictures/{{x.id}}/{{x.fotos[0].url}}" alt="">
							<div class="card-body">
								<div class="card-title"><a href="listings_single.php?id={{x.id}}">{{x.id}} - {{x.titulo}}</a></div>
								<div class="card-text">{{x.direccion}}</div>
								<div class="rooms">

									<div class="room">
										<span class="room_title">Habitaciones</span>
										<div class="room_content">
											<div class="room_image"><img src="images/bedroom.png" alt=""></div>
											<span class="room_number">{{x.cantidad_cuartos}}</span>
										</div>
									</div>

									<div class="room">
										<span class="room_title">Baños</span>
										<div class="room_content">
											<div class="room_image"><img src="images/shower.png" alt=""></div>
											<span class="room_number">{{x.cantidad_banios}}</span>
										</div>
									</div>
									<div class="room">
										<span class="room_title">Area total</span>
										<div class="room_content">
											<div class="room_image"><img src="images/area.png" alt=""></div>
											<span class="room_number">{{x.area_total | number}} m²</span>
										</div>
									</div>
									<div class="room">
										<span class="room_title">Area construída</span>
										<div class="room_content">
											<div class="room_image"><img src="images/area.png" alt=""></div>
											<span class="room_number">{{x.area_construida | number}} m²</span>
										</div>
									</div>

									<div class="room">
										<span class="room_title">Jardín</span>
										<div class="room_content">
											<div class="room_image"><img src="images/patio.png" alt=""></div>
											<span class="room_number">{{x.jardin}}</span>
										</div>
									</div>

									<div class="room">
										<span class="room_title">Cochera</span>
										<div class="room_content">
											<div class="room_image"><img src="images/garage.png" alt=""></div>
											<span class="room_number">{{x.cochera}}</span>
										</div>
									</div>

								</div>

								<div class="room_tags">
									<span class="room_tag" ng-if="x.terraza"><a href="#">Terraza</a></span>
									<span class="room_tag" ng-if="x.cocina_amueblada"><a href="#">Cocina amueblada</a></span>
									<span class="room_tag" ng-if="x.energia_electrica"><a href="#">Energía Eléctrica</a></span>
									<span class="room_tag" ng-if="x.agua"><a href="#">Agua potable</a></span>
								</div>

							</div>
						</div>

						<div class="featured_card_box d-flex flex-row align-items-center trans_300">
							<img src="images/tag.svg" alt="https://www.flaticon.com/authors/lucy-g">
							<div class="featured_card_box_content">
								<div class="featured_card_price_title" ng-if="x.is_negociable == 1">Negociable</div>
								<div class="featured_card_price_title" ng-if="x.is_negociable == 0">No Negociable</div>
								<div class="featured_card_price">{{x.precio | currency}}</div>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- Testimonials -->

	<div class="testimonials">
		<div class="testimonials_background_container prlx_parent">
			<div class="testimonials_background prlx" style="background-image:url(images/testimonials_background.jpg)"></div>
		</div>
		<div class="container">

			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h3>Testimonios de clientes</h3>
						<span class="section_subtitle">Vea los mejores comentarios</span>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					
					<div class="testimonials_slider_container">

						<!-- Testimonials Slider -->
						<div class="owl-carousel owl-theme testimonials_slider">
							
							<!-- Testimonials Item -->
							<div class="owl-item">
								<div class="testimonials_item text-center">
									<p class="testimonials_text">Vencore es lo mejor que me pudo haber pasado, su asesoría me llevo a tomar la mejor decisión de mi vida, mi vivienda hoy es una realidad gracias al profesionalismo de Vencore.</p>
									<div class="testimonial_user">
										<div class="testimonial_image mx-auto">
											<img src="images/person.jpg" alt="https://unsplash.com/@remdesigns">
										</div>
										<div class="testimonial_name">Natalie Smith</div>
										<div class="testimonial_title">Cliente en Estados Unidos</div>
									</div>
								</div>
							</div>

							<!-- Testimonials Item -->
							<div class="owl-item">
								<div class="testimonials_item text-center">
									<p class="testimonials_text">Comprar una vivienda siempre es una decisión importante en la vida de una persona, hoy en día hay tantas casas que se ofrecen pero uno realmente no sabe si están legales o tienen problemas. Por ello me decidí en depositar mi confianza en Vencore porque sabía que no me decepcionarían y me darían mi casa integra.</p>
									<div class="testimonial_user">
										<div class="testimonial_image mx-auto">
											<img src="images/person.jpg" alt="https://unsplash.com/@remdesigns">
										</div>
										<div class="testimonial_name">Nicole Sosa</div>
										<div class="testimonial_title">Cliente en El Salvador</div>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- Workflow -->

	<div class="workflow">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h3>¿Como trabajamos?</h3>
						<span class="section_subtitle">Todo lo que necesitas saber</span>
					</div>
				</div>
			</div>

			<div class="row workflow_row">
				<div class="workflow_rocket"><img src="images/rocket.png" alt=""></div>

				<!-- Workflow Item -->
				<div class="col-lg-4 workflow_col">
					<div class="workflow_item">
						<div class="workflow_image_container d-flex flex-column align-items-center justify-content-center">
							<div class="workflow_image_background">
								<div class="workflow_circle_outer trans_200"></div>
								<div class="workflow_circle_inner trans_200"></div>
								<div class="workflow_num text-center trans_200"><span>01.</span></div>
							</div>
							<div class="workflow_image">
								<img src="images/workflow_1.png" alt="">
							</div>
							
						</div>
						<div class="workflow_item_content text-center">
							<div class="workflow_title">Elija una vivienda</div>
							<p class="workflow_text">Seleccione una vivienda y llamenos por teléfono para preguntar sobre ella, tenga en cuenta el título y precio para que nuestro personal sepa identificar la vivienda que usted desea. O llamenos si le interesa vender una vivienda por medio de nuestro servicio de bienes y raíces.</p>
						</div>
					</div>
				</div>

				<!-- Workflow Item -->
				<div class="col-lg-4 workflow_col">
					<div class="workflow_item">
						<div class="workflow_image_container d-flex flex-column align-items-center justify-content-center">
							<div class="workflow_image_background">
								<div class="workflow_circle_outer trans_200"></div>
								<div class="workflow_circle_inner trans_200"></div>
								<div class="workflow_num text-center trans_200"><span>02.</span></div>
							</div>
							<div class="workflow_image">
								<img src="images/workflow_2.png" alt="">
							</div>
							
						</div>
						<div class="workflow_item_content text-center">
							<div class="workflow_title">Visítenos en nuestras oficinas</div>
							<p class="workflow_text">Para recibir asesoría de manera oportuna puede visitarnos en nuestra dirección 8a Avenida Nte No.3-8 Santa Tecla. La Libertad, le atenderemos con amabilidad y esmero para apoyarle en la venta o adquisición de su vivienda.</p>
						</div>
					</div>
				</div>

				<!-- Workflow Item -->
				<div class="col-lg-4 workflow_col">
					<div class="workflow_item">
						<div class="workflow_image_container d-flex flex-column align-items-center justify-content-center">
							<div class="workflow_image_background">
								<div class="workflow_circle_outer trans_200"></div>
								<div class="workflow_circle_inner trans_200"></div>
								<div class="workflow_num text-center trans_200"><span>03.</span></div>
							</div>
							<div class="workflow_image">
								<img src="images/workflow_3.png" alt="">
							</div>
							
						</div>
						<div class="workflow_item_content text-center">
							<div class="workflow_title">Muevase a una nueva vida</div>
							<p class="workflow_text">Al finalizar la asesoría y completar las fases para la adquisición de su vivienda, usted se sentirá satisfecho y feliz con su nueva vivienda o venta. Vencore cuenta con un servicio de alta calidad y profesionalismo para que usted sienta la confianza necesaria en tan importante decisión.</p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

 

	<!-- Call to Action -->

	<div class="cta_1">
		<div class="cta_1_background" style="background-image:url(images/cta_1.jpg)"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					
					<div class="cta_1_content d-flex flex-lg-row flex-column align-items-center justify-content-start">
						<h3 class="cta_1_text text-lg-left text-center">¿Deseas comunicarte con un experto en bienes y raíces?</span></h3>
						<div class="cta_1_phone">Llame ahora:   +50325104022</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">
				
				<!-- Footer About -->
				<div class="col-lg-3 footer_col">
					<div class="footer_social">
						<ul class="footer_social_list"> 
							<li class="footer_social_item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-twitter"></i></a></li>
						</ul>
					</div>
					 <div class="footer_about">
						<p>Vencore es una empresa salvadoreña con amplia trayectoria en la asesoría de compra y venta de bienes raíces, contamos con una base amplia de clientes que dan fe de nuestro profesionalismo y trabajo dedicado 100% a la satisfacción del servicio que ofrecemos.</p>
					</div>
				</div>
				
				<!-- Footer Useful Links -->

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">Enlaces útiles</div>
					<ul class="footer_useful_links">
						<li class="useful_links_item"><a href="index.php">Home</a></li>
						<li class="useful_links_item"><a href="listings.php">Viviendas</a></li>
						<li class="useful_links_item"><a href="legals.php">Asesoría legal</a></li>
						<li class="useful_links_item"><a href="contact.php">Contacto</a></li>
					</ul>
				</div>

				<!-- Footer Contact Form -->
				<!-- <div class="col-lg-3 footer_col">
					<div class="footer_col_title">Escribanos</div>
					<div class="footer_contact_form_container">
						<form id="footer_contact_form" class="footer_contact_form" action="post">
							<input id="contact_form_name" class="input_field contact_form_name" type="text" placeholder="Nombre" required="required" data-error="Name is required.">
							<input id="contact_form_email" class="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required.">
							<textarea id="contact_form_message" class="text_field contact_form_message" name="message" placeholder="Mensaje" required="required" data-error="Please, write us a message."></textarea>
							<button id="contact_send_btn" type="submit" class="contact_send_btn trans_200" value="Submit">Enviar</button>
						</form>
					</div>
				</div> -->

				<!-- Footer Contact Info -->

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">Información</div>
					<ul class="contact_info_list">
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/placeholder.svg" alt=""></div></div>
							<div class="contact_info_text">8a Avenida Nte No.3-8 Santa Tecla. La Libertad</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/phone-call.svg" alt=""></div></div>
							<div class="contact_info_text">(503) 2510 4022</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/message.svg" alt=""></div></div>
							<div class="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">ventas@vencoremiespacio.com</a></div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src="images/planet-earth.svg" alt=""></div></div>
							<div class="contact_info_text"><a>WhatsApp +50377473063</a></div>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</footer>

	<!-- Credits -->

	<div class="credits">
			<span>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Este portal ha sido desarrollado <i class="fa fa-heart" aria-hidden="true"></i> por <a href="https://adisingenieros.com" target="_blank">ADIS Ingenieros</a>
</span>
	</div>

</div>

<script src="js/main.js?id=2323"></script>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/angular.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/scrollTo/jquery.scrollTo.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script>
<script src="js/vencore.js?id=434"></script>	
</body>

</html>