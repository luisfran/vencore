var app = angular.module("vencore", []);
var appDetail = angular.module("vencore2", ['slickCarousel']);
app.controller("housesController", function($scope, $http) {
    $scope.API = API;
    //$scope.houses = ["Milk", "Bread", "Cheese"];
    $http({
        method: 'GET',
        url: API + 'Vivienda/getViviendasAPI'
    }).then(function successCallback(response) {
        // this callback will be called asynchronously
        // when the response is available
        $scope.pageData = response.data;
        $scope.houses = response.data[0];
        console.log($scope.pageData);
        $scope.pages = response.data;

    }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        console.log(response);
    });

    $scope.selectedPage = function(event) {
        $(event.target).parent().parent().find('li').removeClass('active');
        $(event.target).parent().addClass('active');
        var numberOfPage = $(event.target).html() - 1;
        console.log(numberOfPage);
        $(window).scrollTop(0);
        $scope.houses = $scope.pageData[numberOfPage];
    }

    $scope.search = function(event) {
        console.log(1);
        $http({
            method: 'GET',
            url: API + 'Vivienda/indexFilter/' + $('input[name="estado"]:checked').val() + '/' +
                $("#id_municipio").val() + '/' + $("#tipo").val() + '/' + $("#precio_minimo").val() + '/' + $("#precio_maximo").val()
        }).then(function successCallback(response) {
            // this callback will be called asynchronously
            // when the response is available
            $scope.houses = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            console.log(response);
        });
    }
});

app.controller("featuredHousesController", function($scope, $http) {
    $scope.API = API;
    //$scope.houses = ["Milk", "Bread", "Cheese"];
    $http({
        method: 'GET',
        url: API + 'Vivienda/getFeaturedViviendasAPI'
    }).then(function successCallback(response) {
        // this callback will be called asynchronously
        // when the response is available
        $scope.houses = response.data;
        console.log($scope.houses);
    }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        console.log(response);
    });

    $scope.search = function(event) {
        $http({
            method: 'GET',
            url: API + 'Vivienda/indexFilter/' + $('input[name="estado"]:checked').val() + '/' +
                $("#id_municipio").val() + '/' + $("#tipo").val() + '/' + $("#precio_minimo").val() + '/' + $("#precio_maximo").val()
        }).then(function successCallback(response) {
            // this callback will be called asynchronously
            // when the response is available
            $scope.houses = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            console.log(response);
        });
    }
});


app.controller("municipiosController", function($scope, $http) {
    // Simple GET request example:
    $http({
        method: 'GET',
        url: API + 'Vivienda/getMunicipios'
    }).then(function successCallback(response) {
        // this callback will be called asynchronously
        // when the response is available
        $scope.municipios = response.data;
        console.log($scope.municipios);
    }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        console.log(response);
    });
});

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

appDetail.controller("detailController", function($scope, $http) {
    // Simple GET request example:
    $scope.API = API;
    $http({
        method: 'GET',
        url: API + 'Vivienda/getViviendaDetail/' + findGetParameter('id')
    }).then(function successCallback(response) {
        // this callback will be called asynchronously
        // when the response is available
        $scope.house = response.data;
        $scope.slickConfig = {
            enabled: true,
            autoplay: true,
            draggable: false,
            autoplaySpeed: 1500,
            dots: true,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            method: {},
            event: {
                beforeChange: function(event, slick, currentSlide, nextSlide) {},
                afterChange: function(event, slick, currentSlide, nextSlide) {}
            }
        };
        console.log($scope.house);

    }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        console.log(response);

    });
});