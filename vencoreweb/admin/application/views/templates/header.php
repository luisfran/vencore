<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Vencore mi Espacio</title>
	<link rel="stylesheet"  href="<?php echo base_url()."assets/css/bootstrap.min.css?id=1"; ?>">
		<link rel="stylesheet"  href="<?php echo base_url()."assets/css/style.css?id=2"; ?>">
			<link rel="stylesheet"  href="<?php echo base_url()."assets/css/dropzone.css?id=1"; ?>">

	<script src="<?php echo base_url()."assets/js/jquery-3.1.1.min.js"; ?>"></script>
	<script src="<?php echo base_url()."assets/js/bootstrap.min.js"; ?>"></script>
	<script src="<?php echo base_url()."assets/js/dropzone.js"; ?>"></script> 
</head>
<body>