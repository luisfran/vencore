<div id="container1">
   <div class="panel panel-primary">
      <div class="panel-heading">Agregar Fotografías</div>
      <div class="panel-body">
          <?php if(isset($response)){
               if($insert_id == 0){
               ?>
            <div class="alert alert-danger">
               <strong>Error!</strong> <?php echo $response; ?>
            </div>
            <?php 
               }else if($insert_id != 0){
                  ?>
            <div class="alert alert-success">
               <strong>Hecho!</strong> Registro exitoso, agregue fotos a la vivienda.
            </div>
            <?php
               }
               } ?>
         <form action="<?php echo base_url().'Vivienda/subirFotos' ?>" method="post" enctype="multipart/form-data"  class="dropzone" id="myDropzone">   
          <input type="hidden" value="<?php echo $insert_id; ?>" name="insertId" />
         </form>

         <button type="submit" class="btn btn-primary" id="btnGuardar">Finalizar</button>
      </div>
   </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Registro exitoso</h4>
      </div>
      <div class="modal-body">
        <p>La vivienda se registro satisfactoriamente.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="btnNew">Ok</button>
      </div>
    </div>

  </div>
</div>
<script>
$(document).ready(function(e) { 
    $("#btnNew").on("click", function(e) {
        location.href = '<?php echo base_url()."Vivienda"; ?>';
    });
    $("#btnGuardar").click(function() {
        var myDropzone = Dropzone.forElement(".dropzone");
        myDropzone.processQueue();
    });
});

Dropzone.options.myDropzone = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 100, // MB
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100, // use it with uploadMultiple
        acceptedFiles: ".jpg, .jpeg, .png, .gif",
        addRemoveLinks: true,
        dictDefaultMessage: "Arrastre las imágenes o haga click aquí para subirlas",
        dictInvalidFileType: "Tipo de archivo no válido",
        dictCancelUpload: "Cancelar",
        dictRemoveFile: "Remover",
        accept: function(file, done) {
            done();
        },
        init: function() {
            this.on("success", function(file, responseText) {
                console.log(responseText);
                $('#myModal').modal('show');
            });
        }
};
</script>