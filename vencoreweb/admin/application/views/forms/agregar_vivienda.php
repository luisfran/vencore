<div id="container1">
   <div class="panel panel-primary">
      <div class="panel-heading">Agregar Vivienda</div>
      <div class="panel-body">
         <form method="POST" action="<?php echo base_url()."Vivienda/crear"?>">
            <?php if(isset($response)){
               if($response == "error"){
               ?>
            <div class="alert alert-danger">
               <strong>Error!</strong> <?php echo $response; ?>
            </div>
            <?php 
               }else if($response == "ok"){
                  ?>
            <div class="alert alert-success">
               <strong>Hecho!</strong> Registro agregado exitosamente
            </div>
            <?php
               }
               } ?>
            <div class="form-group">
               <label for="titulo">Título</label>
               <input type="text" class="form-control" id="titulo" name="titulo" aria-describedby="titulo" placeholder="Ingrese título del anuncio">
            </div>
            <div class="form-group">
               <label for="precio">Precio $ (Digitar solo numeros Ej: 14000)</label>
               <input  type="number" step="any" class="form-control" id="precio" name="precio" aria-describedby="precio" placeholder="Ingrese precio de la vivienda">
            </div>
            <div class="form-group">
               <label for="is_negociable">Precio negociable</label>
               <label class="radio-inline"><input type="radio" name="is_negociable" value="1" checked>Sí</label>
               <label class="radio-inline"><input type="radio" name="is_negociable" value="0">No</label>
            </div>
            <div class="form-group">
               <label for="id_departamento">Departamento</label>
               <select class="form-control" id="id_departamento" name="id_departamento">
                  <option value="0">Seleccionar</option>
               <?php 
                  foreach ($departamentos as $row) {
                     echo '<option value ="'.$row->id.'">'.$row->nombre.'</option>';
                  }
                  ?>
               </select>
            </div>
            <div class="form-group">
               <label for="id_municipio">Municipio</label>
               <select class="form-control" id="id_municipio" name="id_municipio">
               <?php 
                  foreach ($municipios as $row) {
                     echo '<option value ="'.$row->id.'">'.$row->nombre.'</option>';
                  }
                  ?>
               </select>
            </div>
            <div class="form-group">
               <label for="direccion">Descripción</label>
               <input type="text" class="form-control" id="direccion" name="direccion" aria-describedby="descripcion" placeholder="Ingrese una descripción para la vivienda">
            </div>
            <div class="form-group">
               <label for="area_total">Area total en m² (Digitar solo números Ej: 5000)</label>
               <input type="text" class="form-control" id="area_total" name="area_total" aria-describedby="area_total" placeholder="Ingrese el área total en metros cuadrados">
            </div>
            <div class="form-group">
               <label for="area_construida">Area construida en m² (Digitar solo números Ej: 5000)</label>
               <input type="text" class="form-control" id="area_construida" name="area_construida" aria-describedby="area_construida" placeholder="Ingrese el área construída en metros cuadrados">
            </div>
            <div class="form-group">
               <label for="cantidad_cuartos">Habitaciones (Digitar solo números Ej: 1)</label>
               <input type="text" class="form-control" id="cantidad_cuartos" name="cantidad_cuartos" aria-describedby="cantidad_cuartos" placeholder="Ingrese la cantidad de habitaciones disponibles">
            </div>
            <div class="form-group">
               <label for="tipo_piso">Tipo de piso</label>
               <select class="form-control" id="tipo_piso" name="tipo_piso">
                  <option value="Cerámica">Cerámica</option>
                  <option value="Cerámica">Ladrillo de piso</option>
                  <option value="Cerámica">Cemento</option>
                  <option value="Cerámica">Tierra</option>
               </select>
            </div>
            <div class="form-group">
               <label for="energia_electrica">Energía eléctrica</label>
               <label class="radio-inline"><input type="radio" name="energia_electrica" value="1">Sí</label>
               <label class="radio-inline"><input type="radio" name="energia_electrica" value="0">No</label>
            </div>
            <div class="form-group">
               <label for="tipo_piso">Agua potable</label>
               <label class="radio-inline"><input type="radio" name="agua" value="1">Sí</label>
               <label class="radio-inline"><input type="radio" name="agua" value="0">No</label>
            </div>
            <div class="form-group">
               <label for="tipo_techo">Tipo de techo</label>
               <select class="form-control" id="tipo_techo" name="tipo_techo">
                  <option value="Cerámica">Madera</option>
                  <option value="Cerámica">Lámina</option>
                  <option value="Cerámica">Teja</option>
                  <option value="Cerámica">Tierra</option>
                  <option value="Cerámica">Vidrio</option>
               </select>
            </div>
            <div class="form-group">
               <label for="cantidad_banios">Baños (Digitar solo números Ej: 1)</label>
               <input type="text" class="form-control" id="cantidad_banios" name="cantidad_banios" aria-describedby="cantidad_banios" placeholder="Ingrese la cantidad de baños disponibles">
            </div>
            <div class="form-group">
               <label for="terraza">Terraza</label>
               <label class="radio-inline"><input type="radio" name="terraza" value="1">Sí</label>
               <label class="radio-inline"><input type="radio" name="terraza" value="0">No</label>
            </div>
            <div class="form-group">
               <label for="cocina_amueblada">Cocina amueblada</label>
               <label class="radio-inline"><input type="radio" name="cocina_amueblada" value="1">Sí</label>
               <label class="radio-inline"><input type="radio" name="cocina_amueblada" value="0">No</label>
            </div>
            <div class="form-group">
               <label for="cochera">Cocheras (Digitar solo números Ej: 1)</label>
               <input type="number" class="form-control" id="cochera" name="cochera" aria-describedby="cochera" placeholder="Ingrese la cantidad de cocheras disponibles">
            </div>
            <div class="form-group">
               <label for="jardin">Jardínes (Digitar solo números Ej: 1)</label>
               <input type="number" class="form-control" id="jardin" name="jardin" aria-describedby="jardin" placeholder="Ingrese la cantidad de jardines disponibles">
            </div>
            <div class="form-group">
               <label for="estado">Estado</label>
               <select class="form-control" id="estado" name="estado">
                  <option value="Nueva">Nueva</option>
                  <option value="Usada">Usada</option>
               </select>
            </div>
            <div class="form-group">
               <label for="id_categoria">Categoría</label>
               <select class="form-control" id="id_categoria" name="id_categoria">
               <?php 
                  foreach ($categorias as $row) {
                     echo '<option value ="'.$row->id.'">'.$row->nombre.'</option>';
                  }
                  ?>
               </select>
            </div>
            <div class="form-group">
               <label for="is_featured">Destacar anuncio en página principal</label>
               <label class="radio-inline"><input type="radio" name="is_featured" value="1">Sí</label>
               <label class="radio-inline"><input type="radio" name="is_featured" value="0" checked>No</label>
            </div>
            <button type="submit" class="btn btn-primary">Siguiente</button>
            <button type="reset" class="btn btn-danger">Cancelar</button>
         </form>
      </div>
   </div>
</div>
<script>
   $(document).ready(function(e){
      $("#id_departamento").on('change', function(){
         $('#id_municipio option').each(function() {
           $(this).remove();
   });
         $.post( '<?php echo base_url()."Vivienda/getMunicipiosByDepartamento/" ?>' + this.value , function( data ) {
           for(var i in data){
                       $('#id_municipio').append($('<option>', {
                value: data[i].id,
                text: data[i].nombre
            }));
           }
         });    
      });
   });
</script>