<div id="container1">
   <div class="panel panel-primary">
      <div class="panel-heading">Buscar usuario</div>
      <div class="panel-body">
        <form method="get" action="<?php echo base_url()."Usuarios/buscarUsuario"; ?>">
          <div class="form-group">
            <div class="input-group">
              <input type="text" name= "value" class="form-control" placeholder="Digite el usuario o Dui ...">
               <span class="input-group-btn">
                <input class="btn btn-default" type="submit" value="Buscar"/>
              </span>
            </div><!-- /input-group -->
          </div>
        </form>
         <table class="table table-bordered">
            <thead>
               <tr>
                  <th>Usuario</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Perfil</th>
                  <th>Editar</th>
                  <th>Eliminar</th>
               </tr>
            </thead>
            <tbody> 
                <?php if(isset($usuarios)){ ?>
                <?php foreach($usuarios as $row) {?>
                  <tr>
                    <td> <?php echo $row->usuario; ?> </td>
                    <td> <?php echo $row->nombres; ?> </td>
                    <td> <?php echo $row->apellidos; ?> </td>
                     <td> <?php
                      if($row->perfil == 1){
                        echo 'Administrador';
                      }else{
                        echo 'Editor';
                      }
                      ?> </td>
                    <td><a href="<?php echo base_url()."Usuarios/detalle/".$row->id; ?>"><button type="button" class="btn btn-info">Editar</button></a></td>
                     <td><input type="hidden" id="rowToDelete" value ="<?php echo $row->id ?>"/> <button type="button" id="btn-modal-delete" class="btn btn-danger">Eliminar</button></a></td>
                  </tr>
                <?php } ?>
                <?php }  ?>
            </tbody>
         </table>
      </div>
   </div>
</div>
<!-- Modal -->
    <div id="modalDialog" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Eliminar</h4>
                </div>
                <div class="modal-body">
                    <p>Esta seguro en eliminar el registro?</p>
                    <input type="hidden" id="data-id"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnEliminar" data-dismiss="modal">Aceptar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">

$(document).on('click','#btn-modal-delete', function(e){
    var id = $(this).parent().find("#rowToDelete").val();
    $('#modalDialog .modal-body #data-id').val(id);
    $('#modalDialog').modal('toggle');
});

$('.modal-dialog .modal-footer').on('click','#btnEliminar', function(e) {
    var id = $('.modal-body #data-id').val();
    $.ajax({
        type: 'POST',
        data: { id: id},
        url: '<?php echo base_url()."Usuarios/eliminar" ?>',
        success: function(data) {
          console.log(data);
          $('#modalDialog').modal('toggle');
          location.reload();
        }
    });
    return false;

});
</script>