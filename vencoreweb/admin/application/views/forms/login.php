 <div id="container1">
  <div id="title"><h2>Ingresar al sistema</h2></div>
  <form method="POST" action="<?php echo base_url()."Dashboard/checkLogin"?>">
   <?php if(isset($invalid_credential)){ ?>
    <div class="alert alert-danger">
      <strong>Error!</strong> <?php echo $invalid_credential; ?>
    </div>
    <?php } ?>
    <div class="form-group">
      <label for="username">Usuario</label>
      <input type="text" class="form-control" id="usuario" required="required" name="usuario" aria-describedby="Usuario" placeholder="Ingresa tu usuario">
    </div>
    <div class="form-group">
      <label for="password">Contraseña</label>
      <input type="password" class="form-control" name="clave" required="required" id="clave" placeholder="Contraseña">
    </div>
    <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
  </form>
</div>
 