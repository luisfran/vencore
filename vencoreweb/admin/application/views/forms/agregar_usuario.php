 <div id="container1">
  <div class="panel panel-primary">
      <div class="panel-heading">Agregar un nuevo usuario</div>
      <div class="panel-body">
      	<form method="POST" action="<?php echo base_url()."Usuarios/agregar"?>">
		   <?php if(isset( $response )){
		   			if($response == "error"){
		    ?>
		    <div class="alert alert-danger">
		      <strong>Error!</strong> <?php echo $response; ?>
		    </div>
		    <?php 
		    		}else if($response == "ok"){
		    			?>
		    			 <div class="alert alert-success">
		     				 <strong>Hecho!</strong> Registro agregado exitosamente
		    			</div>
		    			<?php
		    		}else{ ?>
		    			<div class="alert alert-danger">
		     				 <strong>Error!</strong> <?php echo $response; ?>
		    			</div>
		    <?php		}
			} ?>
		   <div class="form-group">
		      <label for="usuario">Usuario:</label>
		      <input type="text" required="required" class="form-control" id="usuario" name="usuario" aria-describedby="usuario" placeholder="Ingresa el usuario">
		    </div>
		    <div class="form-group">
		      <label for="clave">Contraseña:</label>
		      <input type="password" required="required" class="form-control" id="clave" name="clave" aria-describedby="clave" placeholder="Ingresa la contraseña">
		    </div>
		     <div class="form-group">
		      <label for="clave2">Repetir contraseña:</label>
		      <input type="password" required="required" class="form-control" id="clave2" name="clave2" aria-describedby="clave2" placeholder="Ingresa la contraseña nuevamente">
		    </div>
		    <div class="form-group">
		      <label for="nombres">Nombres:</label>
		      <input type="nombres" required="required" class="form-control" id="nombres" name="nombres" aria-describedby="nombres" placeholder="Ingresa los nombres">
		    </div>
		    <div class="form-group">
		      <label for="apellidos">Apellidos:</label>
		      <input type="apellidos" required="required" class="form-control" id="apellidos" name="apellidos" aria-describedby="apellidos" placeholder="Ingrese los apellidos">
		    </div>
		    <div class="form-group">
		        <label for="perfil">Seleccione el perfil:</label>
				  <select class="form-control" required="required" id="perfil" name="perfil">
				  	<?php foreach($perfiles as $row){?>
				    <option value= "<?php echo $row->id ?>"><?php echo $row->nombre;?></option>
				    <?php }?>
				</select>
		    </div>
		    <button type="submit" class="btn btn-primary">Guardar</button>
		    <button type="reset" class="btn btn-danger">Cancelar</button>
		</form>
      </div>
    </div>
 </div>