<div id="container1">
   <div class="panel panel-primary">
      <div class="panel-heading">Remueve fotografías o agrega más haciendo click en el centro del contenedor de imágenes</div>
      <div class="panel-body">
         <form action="<?php echo base_url().'Vivienda/modificarAlbumDeFotos' ?>" method="post" enctype="multipart/form-data"  class="dropzone" id="myDropzone">   
          <input type="hidden" value="<?php echo $id; ?>" name="id" />
         </form>

         <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar cambios</button>
      </div>
   </div>
</div>
<script>
Dropzone.options.myDropzone = {
  paramName: "file", // The name that will be used to transfer the file
  maxFilesize: 100, // MB
  autoProcessQueue: false,
  uploadMultiple: true,
  parallelUploads: 100, // use it with uploadMultiple
  acceptedFiles: ".jpg, .jpeg, .png, .gif",
  addRemoveLinks: true,
  dictDefaultMessage: "Arrastre las imágenes o haga click aquí para subirlas",
  dictInvalidFileType: "Tipo de archivo no válido",
  dictCancelUpload: "Cancelar",
  dictRemoveFile: "Remover",
  accept: function(file, done) {
    //Event fired when upload image in preview
    done();
    $(".dropzone .dz-preview .dz-image").css({"width":"200px", "height":"200px"});
    $(".dropzone .dz-preview .dz-image img").css({"width":"200px", "height":"200px"});
  },
  init: function() {
  		var thisDropzone = this;

        $.getJSON('<?php echo base_url()."Vivienda/getPhotos/$id" ?>', function(data) { // get the json response
            $.each(data, function(key,value){ //loop through it
                var mockFile = { name: value.url, size: value.size }; // here we get the file name and size as response 
                var imagePath = '<?php echo base_url()."assets/pictures/".$id."/" ?>'+value.url;
                thisDropzone.createThumbnailFromUrl(mockFile,imagePath);
                thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                thisDropzone.options.thumbnail.call(thisDropzone, mockFile, imagePath);//uploadsfolder is the folder where you have all those uploaded files
                $(".dropzone .dz-preview .dz-image").css({"width":"200px", "height":"200px"});
                $(".dropzone .dz-preview .dz-image img").css({"width":"200px", "height":"200px"});
            });

        });
        this.on("success", function(file, responseText) {
            console.log(responseText);
        });
    },
    removedfile: function(file) {
      $.post( '<?php echo base_url()."Vivienda/deletePhoto"; ?>',{ id: '<?php echo $id; ?>', url: file.name }, function( data ) {
        file.previewElement.remove();
      });
    }
};

$("#btnGuardar").click(function(){

    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
});
</script>