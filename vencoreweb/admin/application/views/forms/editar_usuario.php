 <div id="container1">
  <div class="panel panel-primary">
      <div class="panel-heading">Editar usuario</div>
      <div class="panel-body">
      	<form method="POST" action="<?php echo base_url()."Usuarios/editar"?>">
		   <?php if(isset($response)){
		   			if($response == "error"){
		    ?>
		    <div class="alert alert-danger">
		      <strong>Error!</strong> <?php echo $response; ?>
		    </div>
		    <?php 
		    		}else if($response == "ok"){
		    			?>
		    			 <div class="alert alert-success">
		     				 <strong>Hecho!</strong> Registro modificado exitosamente
		    			</div>
		    			<?php
		    		}
			} ?>
		   <div class="form-group">
		   	  <input type = "hidden" id="id" name="id" value="<?php echo $id; ?>"/>
		      <label for="usuario">Usuario</label>
		      <input type="text" required="required" class="form-control" id="usuario" value="<?php echo $usuario; ?>" name="usuario" aria-describedby="usuario" disabled = "disabled">
		      <label for="nombres">Nombres</label>
		      <input type="text" required="required" class="form-control" id="nombres" value="<?php echo $nombres; ?>" name="nombres" aria-describedby="nombres" placeholder="Ingresa los nombres">
		      <label for="apellidos">Apellidos</label>
		      <input type="text" required="required" class="form-control" id="apellidos" value="<?php echo $apellidos; ?>" name="apellidos" aria-describedby="apellidos" placeholder="Ingresa los apellidos">
		      <div class="form-group">
				  <label for="perfil">Perfil:</label>
				  <select class="form-control" id="perfil" name="perfil">
				  	<?php foreach($perfiles as $row){
					?>    
 						 <option value="<?php echo $row->id;?>"><?php echo $row->nombre ?></option>
					<?php  	 
				  	}
				  	?>	
				  </select>
			  </div>
		    </div>
		    <button type="submit" class="btn btn-primary">Guardar</button>
		    <button type="reset" class="btn btn-danger">Cancelar</button>
		</form>
      </div>
    </div>
 </div>
 <script>
 $(document).ready(function (e){
 	 $("#perfil option[value='<?php echo $perfil; ?>']").attr("selected","selected"); 
 });
 </script>