<div id="container1">
   <div class="panel panel-primary">
      <div class="panel-heading">Buscar vivienda</div>
      <div class="panel-body">
        <form method="get" action="<?php echo base_url()."Vivienda/buscarVivienda"; ?>">
          <div class="form-group">
            <div class="input-group">
              <input type="text" name= "param" class="form-control" placeholder="Buscar ..">
               <span class="input-group-btn">
                <input class="btn btn-default" type="submit" value="Buscar"/>
              </span>
            </div><!-- /input-group -->
          </div>
        </form>
         <table class="table table-bordered">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Título</th>
                  <th>Precio</th>
                  <th>Municipio</th>
                  <th>Dirección</th>
                  <th>Area total</th>
                  <th>Editar datos</th>
                  <th>Cambiar fotos</th>
                  <th>Eliminar</th>
               </tr>
            </thead>
            <tbody> 
                <?php if(isset($viviendas)){ ?>
                <?php foreach($viviendas as $row) {?>
                  <tr>
                    <td> <?php echo $row->id; ?> </td>
                    <td> <?php echo $row->titulo; ?> </td>
                    <td> <?php echo $row->precio; ?> </td>
                    <td> <?php echo $row->municipio; ?> </td>
                    <td> <?php echo $row->direccion; ?> </td>
                    <td> <?php echo $row->area_total; ?> </td>
                    <td><a href="<?php echo base_url()."Vivienda/editar/".$row->id; ?>"><button type="button" class="btn btn-primary">Editar datos</button></a></td>
                     <td><a href="<?php echo base_url()."Vivienda/UpdatePhoto/".$row->id; ?>"><button type="button" class="btn btn-info">Cambiar fotos</button></a></td>
                     <td><input type="hidden" id="rowToDelete" value ="<?php echo $row->id ?>"/> <button type="button" id="btn-modal-delete" class="btn btn-danger">Eliminar</button></a></td>
                  </tr>
                <?php } ?>
                <?php }  ?>
            </tbody>
         </table>
      </div>
   </div>
</div>
<!-- Modal -->
    <div id="modalDialog" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Eliminar</h4>
                </div>
                <div class="modal-body">
                    <p>Esta seguro en eliminar el registro?</p>
                    <input type="hidden" id="data-id"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnEliminar" data-dismiss="modal">Aceptar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">

$(document).on('click','#btn-modal-delete', function(e){
    var id = $(this).parent().find("#rowToDelete").val();
    $('#modalDialog .modal-body #data-id').val(id);
    $('#modalDialog').modal('toggle');
});

$('.modal-dialog .modal-footer').on('click','#btnEliminar', function(e) {
    var id = $('.modal-body #data-id').val();
    $.ajax({
        type: 'POST',
        data: { id: id},
        url: '<?php echo base_url()."Vivienda/eliminar" ?>',
        success: function(data) {
          console.log(data);
          $('#modalDialog').modal('toggle');
          location.reload();
        }
    });
    return false;

});
</script>