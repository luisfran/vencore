<style>
   #leftContainer{
   float: left;
   padding-left: 40px;
   padding-right: 40px;
   padding-top: 10px;
   padding-bottom: 5px;
   }
   #rightContainer{
   float: left;
   padding-left: 40px;
   padding-right: 40px;
   padding-top: 10px;
   padding-bottom: 5px;
   }
   .clear{
   clear: both;
   }
   #dvTitle{
   text-align: center;
   }
   #buttonContainer{
   	text-align: center;
   	padding-bottom: 20px;
   }
   .imageContainer img{
   	width: 300px;
   	height: 250px;
   }
   p{
   	font-family: 'Arial, sans-serif';
   	font-size: 1.2em;
   }
</style>
<div id="container1">
   <div id="dvTitle">
      <h2>Vivienda - <?php echo $vivienda[0]->id;?></h2>
   </div>
   <div class="clear"></div>
   <div id="elementsContainer">
      <div id="leftContainer">
         <div>
            <p><?php echo $vivienda[0]->titulo; ?></p>
         </div>
         <div>
            <p>Precio: <?php 
            setlocale(LC_MONETARY, 'en_US');
            echo money_format('%(#10n', $vivienda[0]->precio); if($vivienda[0]->is_negociable){ echo " Negociable";}else{" Fijo";}?></p>
         </div>
         <div>
            <p>Descripción: <?php echo $vivienda[0]->direccion; ?></p>
         </div>
         <div>
            <p>Departamento: <?php echo $vivienda[0]->departamento; ?></p>
         </div>
         <div>
            <p>Municipio: <?php echo $vivienda[0]->municipio; ?></p>
         </div>
         <div>
            <p>Area total (m²): <?php echo number_format($vivienda[0]->area_total); ?></p>
         </div>
         <div>
            <p>Area construida (m²): <?php echo number_format($vivienda[0]->area_construida); ?></p>
         </div>
         <div>
            <p>Habitaciones: <?php echo $vivienda[0]->cantidad_cuartos; ?></p>
         </div>
         <div>
            <p>Estado: <?php echo $vivienda[0]->estado; ?></p>
         </div>
      </div>
      <div id="rightContainer">
         <div>
            <p>Tipo de piso: <?php echo $vivienda[0]->tipo_piso; ?></p>
         </div>
         <div>
            <p>Energía Eléctrica: <?php if($vivienda[0]->energia_electrica){echo "Si";}else{echo "No";}; ?></p>
         </div>
         <div>
            <p>Agua potable: <?php if($vivienda[0]->agua){echo "Si";}else{echo "No";}; ?></p>
         </div>
         <div>
            <p>Tipo de techo: <?php echo $vivienda[0]->tipo_techo; ?></p>
         </div>
         <div>
            <p>Baños: <?php echo $vivienda[0]->cantidad_banios; ?></p>
         </div>
         <div>
            <p>Terraza: <?php if($vivienda[0]->terraza){echo "Si";}else{echo "No";}; ?></p>
         </div>
         <div>
            <p>Cocina amueblada: <?php if($vivienda[0]->cocina_amueblada){echo "Si";}else{echo "No";}; ?></p>
         </div>
         <div>
            <p>Cocheras: <?php echo $vivienda[0]->cochera; ?></p>
         </div>
         <div>
            <p>Jardínes: <?php echo $vivienda[0]->jardin; ?></p>
         </div>
      </div>
   </div>
    <div class="clear"></div>
    <div class="imageContainer">
    	<?php foreach ($vivienda[0]->fotos as $foto) {
    		?>
    	 <img src="<?php echo base_url().'assets/pictures/'.$vivienda[0]->id.'/'.$foto->url ?>"/>
    	<?php } ?>
    </div>
</div>
<div id="buttonContainer">
	<button class="btn btn-primary" id="btnImprimir" class="printElement">Imprimir informe</button>
</div>
<script type="text/javascript">
	$("#btnImprimir").on("click", function(e){
		window.print();
	});
</script>