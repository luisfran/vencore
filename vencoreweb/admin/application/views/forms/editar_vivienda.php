<div id="container1">
   <div class="panel panel-primary">
      <div class="panel-heading">Editar Vivienda</div>
      <div class="panel-body">
         <form method="POST" action="<?php echo base_url()."Vivienda/modificar"?>">
            <?php if(isset($response)){
               if($response == "error"){
               ?>
            <div class="alert alert-danger">
               <strong>Error!</strong> <?php echo $response; ?>
            </div>
            <?php 
               }else if($response == "ok"){
                  ?>
            <div class="alert alert-success">
               <strong>Hecho!</strong> Registro editado exitosamente
            </div>
            <?php
               }
               } ?>
            <input type="hidden" name="id" value="<?php echo $vivienda->id; ?>"/>
            <div class="form-group">
               <label for="titulo">Título</label>
               <input type="text" class="form-control" id="titulo" name="titulo" aria-describedby="titulo" placeholder="Ingrese título del anuncio" value="<?php echo $vivienda->titulo; ?>" required>
            </div>
            <div class="form-group">
               <label for="precio">Precio $</label>
               <input  type="number" min="1" step="any" class="form-control" id="precio" name="precio" aria-describedby="precio" placeholder="Ingrese precio de la vivienda" value="<?php echo $vivienda->precio; ?>" required>
            </div>
            <div class="form-group">
               <label for="id_departamento">Departamento</label>
               <select class="form-control" id="id_departamento" name="id_departamento" required>
               <?php 
                  foreach ($departamentos as $row) {
                     echo '<option value ="'.$row->id.'">'.$row->nombre.'</option>';
                  }
                  ?>
               </select>
            </div>
            <div class="form-group">
               <label for="id_municipio">Municipio</label>
               <select class="form-control" id="id_municipio" name="id_municipio" required>
               <?php 
                  foreach ($municipios as $row) {
                     echo '<option value ="'.$row->id.'">'.$row->nombre.'</option>';
                  }
                  ?>
               </select>
            </div>
            <div class="form-group">
               <label for="direccion">Dirección</label>
               <input type="text" class="form-control" id="direccion" name="direccion" aria-describedby="direccion" placeholder="Ingrese la dirección de vivienda" value="<?php echo $vivienda->direccion; ?>">
            </div>
            <div class="form-group">
               <label for="area_total">Area total</label>
               <input type="text" class="form-control" id="area_total" name="area_total" aria-describedby="area_total" placeholder="Ingrese el área total en metros cuadrados" value="<?php echo $vivienda->area_total; ?>">
            </div>
            <div class="form-group">
               <label for="area_construida">Area construida</label>
               <input type="text" class="form-control" id="area_construida" name="area_construida" aria-describedby="area_construida" placeholder="Ingrese el área construída en metros cuadrados" value="<?php echo $vivienda->area_construida; ?>">
            </div>
            <div class="form-group">
               <label for="cantidad_cuartos">Cantidad de cuartos</label>
               <input type="text" class="form-control" id="cantidad_cuartos" name="cantidad_cuartos" aria-describedby="cantidad_cuartos" placeholder="Ingrese la cantidad de cuartos disponibles" value="<?php echo $vivienda->cantidad_cuartos; ?>">
            </div>
            <div class="form-group">
               <label for="tipo_piso">Tipo de piso</label>
               <select class="form-control" id="tipo_piso" name="tipo_piso">
                  <option value="Cerámica">Cerámica</option>
                  <option value="Cerámica">Ladrillo de piso</option>
                  <option value="Cerámica">Cemento</option>
                  <option value="Cerámica">Tierra</option>
               </select>
            </div>
            <div class="form-group">
               <label for="energia_electrica">Energía eléctrica</label>
               <label class="radio-inline"><input type="radio" id="energia_electrica1" name="energia_electrica" value="1">Sí</label>
               <label class="radio-inline"><input type="radio" id="energia_electrica2" name="energia_electrica" value="0">No</label>
            </div>
            <div class="form-group">
               <label for="tipo_piso">Agua potable</label>
               <label class="radio-inline"><input type="radio" id="agua1" name="agua" value="1">Sí</label>
               <label class="radio-inline"><input type="radio" id="agua2" name="agua" value="0">No</label>
            </div>
            <div class="form-group">
               <label for="tipo_techo">Tipo de techo</label>
               <select class="form-control" id="tipo_techo" name="tipo_techo">
                  <option value="Cerámica">Madera</option>
                  <option value="Cerámica">Lámina</option>
                  <option value="Cerámica">Teja</option>
                  <option value="Cerámica">Tierra</option>
                  <option value="Cerámica">Vidrio</option>
               </select>
            </div>
            <div class="form-group">
               <label for="cantidad_banios">Cantidad de baños</label>
               <input type="text" class="form-control" id="cantidad_banios" name="cantidad_banios" aria-describedby="cantidad_banios" placeholder="Ingrese la cantidad de baños disponibles" value="<?php echo $vivienda->cantidad_banios; ?>">
            </div>
            <div class="form-group">
               <label for="terraza">Terraza</label>
               <label class="radio-inline"><input type="radio" name="terraza" id="terraza1" value="1">Sí</label>
               <label class="radio-inline"><input type="radio" name="terraza" id="terraza2" value="0" checked>No</label>
            </div>
            <div class="form-group">
               <label for="cocina_amueblada">Cocina amueblada</label>
               <label class="radio-inline"><input type="radio" name="cocina_amueblada" id="cocina_amueblada_1" value="1">Sí</label>
               <label class="radio-inline"><input type="radio" name="cocina_amueblada" id="cocina_amueblada_2" value="0">No</label>
            </div>
            <div class="form-group">
               <label for="cochera">Cantidad de cocheras</label>
               <input type="number" class="form-control" id="cochera" name="cochera" aria-describedby="cochera" placeholder="Ingrese la cantidad de cocheras disponibles" value="<?php echo $vivienda->cochera; ?>">
            </div>
            <div class="form-group">
               <label for="jardin">Cantidad de jardínes</label>
               <input type="number" class="form-control" id="jardin" name="jardin" aria-describedby="jardin" placeholder="Ingrese la cantidad de jardines disponibles" value="<?php echo $vivienda->jardin; ?>">
            </div>
            <div class="form-group">
               <label for="estado">Estado</label>
               <select class="form-control" id="estado" name="estado">
                  <option value="Nueva">Nueva</option>
                  <option value="Usada">Usada</option>
               </select>
            </div>
            <div class="form-group">
               <label for="id_categoria">Categoría</label>
               <select class="form-control" id="id_categoria" name="id_categoria">
               <?php 
                  foreach ($categorias as $row) {
                     echo '<option value ="'.$row->id.'">'.$row->nombre.'</option>';
                  }
                  ?>
               </select>
            </div>
            <div class="form-group">
               <label for="is_featured">Destacar anuncio en página principal</label>
               <label class="radio-inline"><input type="radio" id="is_featured1" name="is_featured" value="1">Sí</label>
               <label class="radio-inline"><input type="radio" id="is_featured2" name="is_featured" value="0" checked>No</label>
            </div>
            <button type="submit" class="btn btn-primary">Guardar cambios</button>
         </form>
      </div>
   </div>
</div>
<script>
$(document).ready(function(e) {
    $("#id_departamento").on('change', function() {
        $('#id_municipio option').each(function() {
            $(this).remove();
        });
        $.post('<?php echo base_url()."Vivienda/getMunicipiosByDepartamento/" ?>' + this.value, function(data) {
            for (var i in data) {
                $('#id_municipio').append($('<option>', {
                    value: data[i].id,
                    text: data[i].nombre
                }));
            }
        });
    });

    $.post('<?php echo base_url()."Vivienda/getDepartamentoByMunicipio/".$vivienda->id_municipio ?>', function(data) {
        $("#id_departamento option[value='" + data[0].id + "']").attr("selected", "selected");
        $.post('<?php echo base_url()."Vivienda/getMunicipiosByDepartamento/" ?>' + data[0].id, function(data) {
            for (var i in data) {
                $('#id_municipio').append($('<option>', {
                    value: data[i].id,
                    text: data[i].nombre
                }));
            }
            $("#id_municipio option[value='<?php echo $vivienda->id_municipio; ?>']").attr("selected", "selected");
        });
    });
    //Configurando selects
    $("#estado option[value='<?php echo $vivienda->estado; ?>']").attr("selected", "selected");
    $("#tipo_piso option[value='<?php echo $vivienda->tipo_piso; ?>']").attr("selected", "selected");
    $("#tipo_techo option[value='<?php echo $vivienda->tipo_techo; ?>']").attr("selected", "selected");
    $("#id_categoria option[value='<?php echo $vivienda->id_categoria; ?>']").attr("selected", "selected");
    //Configurando radio buttons
    var cocina = '<?php echo $vivienda->cocina_amueblada;?>';
    if (cocina == 1) {
        $("#cocina_amueblada_1").attr("checked", "checked");
    } else {
        $("#cocina_amueblada_2").attr("checked", "checked");
    }

    var terraza = '<?php echo $vivienda->terraza; ?>';
    if (terraza == 1) {
        $("#terraza1").attr("checked", "checked");
    } else {
        $("#terraza2").attr("checked", "checked");
    }

    var energia = '<?php echo $vivienda->energia_electrica; ?>';
    if (energia == 1) {
        $("#energia_electrica1").attr("checked", "checked");
    } else {
        $("#energia_electrica2").attr("checked", "checked");
    }

    var agua = '<?php echo $vivienda->agua; ?>';
    if (agua == 1) {
        $("#agua1").attr("checked", "checked");
    } else {
        $("#agua2").attr("checked", "checked");
    }

    var featured = '<?php echo $vivienda->is_featured; ?>';
    if (featured == 1) {
        $("#is_featured1").attr("checked", "checked");
    } else {
        $("#is_featured2").attr("checked", "checked");
    }
});
</script>