<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()
	{
		$this->load->view('templates/header');
		$this->load->view('forms/login');
		$this->load->view('templates/footer');
	}

	public function error(){
		$data = array('invalid_credential' => "Usuario o contraseña no válida");
		$this->load->view('templates/header');
		$this->load->view('forms/login', $data);
		$this->load->view('templates/footer');
	}

	public function logout(){
		// destroy session
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
?>