<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Vivienda extends CI_Controller
{
    public function index()
    {
        $this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
            redirect(base_url());
        }
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->model('Vivienda_model');
        $data["departamentos"] = $this->Vivienda_model->getDepartamentos();
        $data["categorias"]    = $this->Vivienda_model->getCategorias();
        $this->load->view('forms/agregar_vivienda', $data);
        $this->load->view('templates/footer');
    }
    
    public function addPhoto()
    {
        $this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
            redirect(base_url());
        }
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $data["response"] = "ok";
        $this->load->view('forms/agregar_vivienda_fotos', $data);
        $this->load->view('templates/footer');
    }

     public function UpdatePhoto($id)
    {
        $this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
            redirect(base_url());
        }
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $data["id"] = $id;
        $this->load->view('forms/editar_vivienda_fotos', $data);
        $this->load->view('templates/footer');
    }

    public function getPhotos($id_vivienda){
    	 $this->load->model('Vivienda_model');
    	 $response = $this->Vivienda_model->getFotosVivienda($id_vivienda);
    	 header('Content-Type: application/json');
    	 echo json_encode($response);
    }

    public function deletePhoto(){
    	$id = $this->input->post('id');
    	$name = $this->input->post('url');
    	$this->load->model('Vivienda_model');
    	$response = $this->Vivienda_model->deletePhoto($id, $name);
    	echo $response;
    }
    
    public function subirFotos()
    {
        if (!empty($_FILES)) {
            $storeFolder = $_SERVER['DOCUMENT_ROOT'] . '/vencore/vencoreweb/admin/assets/pictures/';
            $directory   = $storeFolder . $this->input->post('insertId');
            if (!file_exists($directory)) {
                mkdir($directory, 0777, true);
            }
            
            foreach ($_FILES['file']['tmp_name'] as $key => $value) {
                $tempFile   = $_FILES['file']['tmp_name'][$key];
                $targetFile = $directory ."/". $_FILES['file']['name'][$key];
                if(move_uploaded_file($tempFile, $targetFile)){
                	 $foto = $_FILES['file']['name'][$key];
                	 $size = $_FILES['file']['size'][$key];
                	 $this->load->model('Vivienda_model');
                	 $this->Vivienda_model->agregarFoto($foto, $size , $this->input->post('insertId'));
                }
            }
        } else {
            redirect(base_url() . "Vivienda/crear");
        }
    }

      public function modificarAlbumDeFotos()
    {
        if (!empty($_FILES)) {
            $storeFolder = $_SERVER['DOCUMENT_ROOT'] . '/vencore/vencoreweb/admin/assets/pictures/';
            $directory   = $storeFolder . $this->input->post('id');
            foreach ($_FILES['file']['tmp_name'] as $key => $value) {
                $tempFile   = $_FILES['file']['tmp_name'][$key];
                $targetFile = $directory ."/". $_FILES['file']['name'][$key];
                if(move_uploaded_file($tempFile, $targetFile)){
                	 $foto = $_FILES['file']['name'][$key];
                	 $size = $_FILES['file']['size'][$key];
                	 $this->load->model('Vivienda_model');
                	 $this->Vivienda_model->modificarAlbumDeFotos($foto, $size , $this->input->post('id'));
                }
            }
        } else {
            redirect(base_url() . "Vivienda/UpdatePhoto/".$this->input->post('id'));
        }
    }
    
    public function crear()
    {
        $titulo            = $this->input->post('titulo');
        $precio            = $this->input->post('precio');
        $id_municipio      = $this->input->post('id_municipio');
        $direccion         = $this->input->post('direccion');
        $area_total        = $this->input->post('area_total');
        $area_construida   = $this->input->post('area_construida');
        $cantidad_cuartos  = $this->input->post('cantidad_cuartos');
        $tipo_piso         = $this->input->post('tipo_piso');
        $energia_electrica = $this->input->post('energia_electrica');
        $agua              = $this->input->post('agua');
        $tipo_techo        = $this->input->post('tipo_techo');
        $cantidad_banios   = $this->input->post('cantidad_banios');
        $terraza           = $this->input->post('terraza');
        $cocina_amueblada  = $this->input->post('cocina_amueblada');
        $cochera           = $this->input->post('cochera');
        $jardin            = $this->input->post('jardin');
        $estado            = $this->input->post('estado');
        $id_categoria      = $this->input->post('id_categoria');
        $is_featured       = $this->input->post('is_featured');
        $is_negociable     = $this->input->post('is_negociable');
        $this->load->model('Vivienda_model');
        $data["insert_id"] = $this->Vivienda_model->agregar($titulo, $precio, $id_municipio, $direccion, $area_total, $area_construida, $cantidad_cuartos, $tipo_piso, $energia_electrica, $agua, $tipo_techo, $cantidad_banios, $terraza, $cocina_amueblada, $cochera, $jardin, $estado, $id_categoria, $is_featured, $is_negociable);
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('forms/agregar_vivienda_fotos', $data);
        $this->load->view('templates/footer');
    }
    
    public function buscarVivienda()
    {
        $this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
            redirect(base_url());
        }
        $param = $this->input->get('param');
        $this->load->model('Vivienda_model');
        $data["viviendas"] = $this->Vivienda_model->getDetalle($param);
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('forms/buscar_vivienda', $data);
        $this->load->view('templates/footer');
    }
    
    public function modificar()
    {
    	$id                = $this->input->post('id');
        $titulo            = $this->input->post('titulo');
        $precio            = $this->input->post('precio');
        $id_municipio      = $this->input->post('id_municipio');
        $direccion         = $this->input->post('direccion');
        $area_total        = $this->input->post('area_total');
        $area_construida   = $this->input->post('area_construida');
        $cantidad_cuartos  = $this->input->post('cantidad_cuartos');
        $tipo_piso         = $this->input->post('tipo_piso');
        $energia_electrica = $this->input->post('energia_electrica');
        $agua              = $this->input->post('agua');
        $tipo_techo        = $this->input->post('tipo_techo');
        $cantidad_banios   = $this->input->post('cantidad_banios');
        $terraza           = $this->input->post('terraza');
        $cocina_amueblada  = $this->input->post('cocina_amueblada');
        $cochera           = $this->input->post('cochera');
        $jardin            = $this->input->post('jardin');
        $estado            = $this->input->post('estado');
        $id_categoria      = $this->input->post('id_categoria');
        $is_featured       = $this->input->post('is_featured');
        $this->load->model('Vivienda_model');
        $data["response"] = $this->Vivienda_model->modificar($id, $titulo, $precio, $id_municipio, $direccion, $area_total, $area_construida, $cantidad_cuartos, $tipo_piso, $energia_electrica, $agua, $tipo_techo, $cantidad_banios, $terraza, $cocina_amueblada, $cochera, $jardin, $estado, $id_categoria, $is_featured);
        $data["departamentos"] = $this->Vivienda_model->getDepartamentos();
        $data["categorias"]    = $this->Vivienda_model->getCategorias();
        $data["vivienda"] = $this->Vivienda_model->getViviendaById($id);
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('forms/editar_vivienda', $data);
        $this->load->view('templates/footer');
    }
    
    public function editar($id)
    {
        $this->load->model('Vivienda_model');
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $data["departamentos"] = $this->Vivienda_model->getDepartamentos();
        $data["categorias"]    = $this->Vivienda_model->getCategorias();
        $data["vivienda"] = $this->Vivienda_model->getViviendaById($id);
        $this->load->view('forms/editar_vivienda', $data);
        $this->load->view('templates/footer');
    }
    
    public function buscar()
    {
        $this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
            redirect(base_url());
        }
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('forms/buscar_vivienda');
        $this->load->view('templates/footer');
    }

    public function nuevoInforme(){
        $this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
            redirect(base_url());
        }
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('forms/buscar_vivienda_informe');
        $this->load->view('templates/footer');
    }

    public function generarInforme($id_vivienda){
        $this->load->model('Vivienda_model');
        $data["vivienda"] = $this->Vivienda_model->getViviendaDetail2($id_vivienda);
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('forms/informe_vivienda', $data);
        $this->load->view('templates/footer');
    }

       public function buscarViviendaInforme()
    {
        $this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
            redirect(base_url());
        }
        $param = $this->input->get('param');
        $this->load->model('Vivienda_model');
        $data["viviendas"] = $this->Vivienda_model->getDetalle($param);
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('forms/buscar_vivienda_informe', $data);
        $this->load->view('templates/footer');
    }
   
    
    public function getIdVivienda()
    {
        $numero   = $this->input->post('numero');
        $poligono = $this->input->post('poligono');
        $this->load->model('Vivienda_model');
        $response = $this->Vivienda_model->getIdVivienda($numero, $poligono);
        echo json_encode($response);
    }
    
    public function getMunicipios()
    {
        $this->load->model('Vivienda_model');
        $response = $this->Vivienda_model->getMunicipios();
        echo json_encode($response);
    }
    
    public function getDepartamentos()
    {
        $this->load->model('Vivienda_model');
        $response = $this->Vivienda_model->getDepartamentos();
        echo json_encode($response);
    }
    
    public function getMunicipiosByDepartamento($id_departamento)
    {
        $this->load->model('Vivienda_model');
        $response = $this->Vivienda_model->getMunicipiosByDepartamento($id_departamento);
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function getDepartamentoByMunicipio($id_municipio){
        $this->load->model('Vivienda_model');
        $response = $this->Vivienda_model->getDepartamentoByMunicipio($id_municipio);
        header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    public function getViviendas($param)
    {
        $this->load->model('Vivienda_model');
        $response = $this->Vivienda_model->getViviendas();
        echo json_encode($response);
    }

    public function eliminar(){
		$this->load->model('Vivienda_model');
		$id = $this->input->post('id');
		$response = $this->Vivienda_model->eliminar($id);
		echo $response;
	}

    public function getViviendasAPI(){
         $this->load->model('Vivienda_model');
        $response = $this->Vivienda_model->getViviendasAPI();
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function getFeaturedViviendasAPI(){
        $this->load->model('Vivienda_model');
        $response = $this->Vivienda_model->getFeaturedViviendas();
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function indexFilter($estado, $id_municipio, $tipo, $precio_minimo, $precio_maximo){
        $this->load->model('Vivienda_model');
        $response = $this->Vivienda_model->filterViviendas($estado, $id_municipio, $tipo, $precio_minimo, $precio_maximo);
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function getViviendaDetail($id){
        $this->load->model('Vivienda_model');
        $response = $this->Vivienda_model->getViviendaDetail($id);
        header('Content-Type: application/json');
        echo json_encode($response);
    }
}

?>