<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function index()
    {
        $this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
        	redirect(base_url());
        }
        $this->load->view('templates/header');
        $this->load->view('templates/menu');
        $this->load->view('forms/dashboard');
        $this->load->view('templates/footer');
    }
    
    public function checkLogin()
    {
        $this->load->model('Usuario_model');
        $row = $this->Usuario_model->findUsuario($this->input->post("usuario"), $this->input->post("clave"));
        if ($row != "") {
            $this->load->library('session');
            $this->session->set_userdata('vencore_access', 'valid');
            redirect(base_url() . "Dashboard");
        } else {
            $reason = "Usuario o contraseña incorrecta.";
            $this->loginError($reason);
        }
    }
    
    public function loginError($reason)
    {
        $data["invalid_credential"] = $reason;
        $this->load->view('templates/header');
        $this->load->view('forms/login', $data);
        $this->load->view('templates/footer');
    }
}
?>