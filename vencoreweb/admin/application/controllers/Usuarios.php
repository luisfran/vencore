<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function index()
	{
		$this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
        	redirect(base_url());
        }
		$this->load->view('templates/header');
		$this->load->view('forms/iniciar_sesion');
		$this->load->view('templates/footer');
	}

	public function agregarUsuario(){
		$this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
        	redirect(base_url());
        }
		$this->load->model('Usuario_model');
		$data["perfiles"] = $this->Usuario_model->getPerfiles();
		$this->load->view('templates/header');
		$this->load->view('templates/menu', $data);
		$this->load->view('forms/agregar_usuario', $data);
		$this->load->view('templates/footer');
	}

	public function cerrar(){
		$array_items = array('usuario', 'nombres');
		$this->session->unset_userdata($array_items);
		$this->index();
	}

	public function agregar()
	{
		$this->load->model('Usuario_model');
		$data["perfiles"] = $this->Usuario_model->getPerfiles();
		if($this->input->post('clave') == $this->input->post('clave2')){
			$data["response"] = $this->Usuario_model->agregar($this->input->post('usuario'), 
			$this->input->post('clave'), 
			$this->input->post('nombres'), 
			$this->input->post('apellidos'), 
			$this->input->post('perfil')
			);
		}else{
			$data["response"] = "Las contraseñas no coinciden";
		}
		$this->load->view('templates/header');
		$this->load->view('templates/menu', $data);
		$this->load->view('forms/agregar_usuario', $data);
		$this->load->view('templates/footer');
	}

	public function buscar(){
		$this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
        	redirect(base_url());
        }
		$this->load->model('Usuario_model');
	 	$this->load->view('templates/header');
		$this->load->view('templates/menu');
		$this->load->view('forms/buscar_usuario');
	    $this->load->view('templates/footer');
	}

	public function buscarUsuario(){
		$this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
        	redirect(base_url());
        }
		$this->load->model('Usuario_model');
		$data["usuarios"] = $this->Usuario_model->getUsuario($this->input->get('value'));
		$this->load->view('templates/header');
		$this->load->view('templates/menu', $data);
	    $this->load->view('forms/buscar_usuario', $data);
	    $this->load->view('templates/footer');
	}

	public function detalle($id){
		$this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
        	redirect(base_url());
        }
		$this->load->model('Usuario_model');
		$data["perfiles"] = $this->Usuario_model->getPerfiles();
		$usuarios = $this->Usuario_model->getUsuarioById($id);
		foreach($usuarios as $row){
			$data["id"] = $row->id;
			$data["usuario"] = $row->usuario;	
			$data["nombres"] = $row->nombres;
			$data["apellidos"] = $row->apellidos;
			$data["perfil"] = $row->perfil;
		}
		$this->load->view('templates/header');
		$this->load->view('templates/menu', $data);
	    $this->load->view('forms/editar_usuario', $data);
	    $this->load->view('templates/footer');
	}


	public function detalle2($id){
		$this->load->library('session');
        if ($this->session->userdata("vencore_access") != 'valid') {
        	redirect(base_url());
        }
		$this->load->model('Usuario_model');
		$data["perfiles"] = $this->Usuario_model->getPerfiles();
		$data["response"] = "ok";
		$usuarios = $this->Usuario_model->getUsuarioById($id);
		foreach($usuarios as $row){
			$data["id"] = $row->id;
			$data["usuario"] = $row->usuario;	
			$data["nombres"] = $row->nombres;
			$data["apellidos"] = $row->apellidos;
			$data["perfil"] = $row->perfil;
		}
		$this->load->view('templates/header');
		$this->load->view('templates/menu', $data);
	    $this->load->view('forms/editar_usuario', $data);
	    $this->load->view('templates/footer');
	}

	public function editar(){
		$this->load->model('Usuario_model');
		$data["response"] = $this->Usuario_model->editar(
			$this->input->post("id"),
			$this->input->post('nombres'), 
			$this->input->post('apellidos'),  
			$this->input->post('perfil'));
		$this->detalle2($this->input->post("id"));
	}

	public function eliminar(){
		$this->load->model('Usuario_model');
		$id = $this->input->post('id');
		$response = $this->Usuario_model->eliminar($id);
		echo $response;
	}

	 public function APIBuscar(){
	 	$this->load->model('Usuario_model');
		$response = $this->Usuario_model->findUsuario($this->input->post('usuario'), $this->input->post('clave'));
		echo json_encode($response);
	 }


	
}
