<?php class Vivienda_model extends CI_Model {
	public $titulo;
	public $precio;
  public $id_municipio;
  public $direccion;
  public $area_total;
  public $area_construida;
  public $cantidad_cuartos;
  public $tipo_piso;
  public $energia_electrica;
  public $agua;
  public $tipo_techo;
  public $cantidad_banios;
  public $terraza;
  public $cocina_amueblada;
  public $cochera;
  public $jardin;
  public $estado;
  public $id_categoria;
  public $is_featured;
  public $is_negociable;



	public function __construct(){
    	// Call the CI_Model constructor
        parent::__construct();
    }

   	public function agregar($titulo, $precio, $id_municipio, $direccion, $area_total, $area_construida, $cantidad_cuartos, $tipo_piso, $energia_electrica, $agua, $tipo_techo, $cantidad_banios, $terraza, $cocina_amueblada, $cochera, $jardin, $estado, $id_categoria, $is_featured, $is_negociable){
   		$this->titulo = $titulo;
   		$this->precio = $precio;
      $this->id_municipio = $id_municipio;
      $this->direccion =  $direccion;
      $this->area_total = $area_total;
      $this->area_construida = $area_construida;
      $this->cantidad_cuartos = $cantidad_cuartos;
      $this->tipo_piso = $tipo_piso;
      $this->energia_electrica = $energia_electrica;
      $this->agua = $agua;
      $this->tipo_techo = $tipo_techo;
      $this->cantidad_banios = $cantidad_banios;
      $this->terraza = $terraza;
      $this->cocina_amueblada = $cocina_amueblada;
      $this->cochera = $cochera;
      $this->jardin = $jardin;
      $this->estado = $estado;
      $this->id_categoria = $id_categoria;
      $this->is_featured = $is_featured;
      $this->is_negociable = $is_negociable;

   		$this->db->insert('vivienda', $this);
      $insert_id = $this->db->insert_id();
   		if($this->db->affected_rows() > 0){
            return $insert_id;
        }else{
            return "error";
        }
   	}

    public function agregarFoto($url, $size, $id_vivienda){
      $data = array(
         'url' => $url ,
         'size' => $size,
         'id_vivienda' => $id_vivienda
      );

      $this->db->insert('fotos', $data);
    }


    public function modificar($id, $titulo, $precio, $id_municipio, $direccion, $area_total, $area_construida, $cantidad_cuartos, $tipo_piso, $energia_electrica, $agua, $tipo_techo, $cantidad_banios, $terraza, $cocina_amueblada, $cochera, $jardin, $estado, $id_categoria, $is_featured){
      $this->titulo = $titulo;
      $this->precio = $precio;
      $this->id_municipio = $id_municipio;
      $this->direccion =  $direccion;
      $this->area_total = $area_total;
      $this->area_construida = $area_construida;
      $this->cantidad_cuartos = $cantidad_cuartos;
      $this->tipo_piso = $tipo_piso;
      $this->energia_electrica = $energia_electrica;
      $this->agua = $agua;
      $this->tipo_techo = $tipo_techo;
      $this->cantidad_banios = $cantidad_banios;
      $this->terraza = $terraza;
      $this->cocina_amueblada = $cocina_amueblada;
      $this->cochera = $cochera;
      $this->jardin = $jardin;
      $this->estado = $estado;
      $this->id_categoria = $id_categoria;
      $this->is_featured = $is_featured;
      $query = $this->db->update('vivienda', $this, array('id' => $id));
      if($query > 0){
            return "ok";
        }else{
            return "error";
        }
    }

    public function modificarAlbumDeFotos($url, $size, $id){
       $data = array(
         'url' => $url ,
         'size' => $size,
         'id_vivienda' => $id
      );
      $this->db->insert('fotos', $data);
    }

    public function deletePhoto($id, $name){
      $this->db->where('id_vivienda' , $id);
      $this->db->where('url', $name);
      return $this->db->delete('fotos');
    }


   	public function getViviendas(){
   		  $query = $this->db->get('vivienda');
        return $query->result();
   	}

    public function getViviendasAPI(){
       $items = 5;
       $totalRows = $this->db->count_all_results('vivienda');
       $numberOfPages = $totalRows/$items; //Modificar numero divisible para la cantidad de items por pagina ahorita son 2
       $initial_limit = 0;
       $lastLimit = $items;
       $pages = [];
       for($i = 0; $i < $numberOfPages; $i++){
            if($i < $numberOfPages){
              $query = $this->db->query("SELECT * FROM vivienda LIMIT $initial_limit,$lastLimit");
            }else{
              //Si es la ultima pagina traer todos los ultimos registros
              $query = $this->db->query("SELECT * FROM vivienda LIMIT $initial_limit");
            }
            $result = $query->result();
            //echo "SELECT * FROM vivienda LIMIT $initial_limit,$lastLimit";
            foreach($query->result() as $key=>$row){
                $fotos = $this->db->query("SELECT * FROM fotos WHERE id_vivienda = $row->id;");
                $result[$key]->fotos = $fotos->result();
            }
            $initial_limit += $items ;
            $pages[$i] = $result;
        }
          return $pages;
       
    }

    public function getFeaturedViviendas(){
      $query= $this->db->query("SELECT * FROM vivienda WHERE is_featured = 1");
      $result = $query->result();
      foreach($query->result() as $key=>$row){
                $fotos = $this->db->query("SELECT * FROM fotos WHERE id_vivienda = $row->id;");
                $result[$key]->fotos = $fotos->result();
      }
      return $result;
    }

    public function filterViviendas($estado, $id_municipio, $tipo, $precio_minimo, $precio_maximo){

      $sql = "SELECT * FROM vivienda WHERE estado = '$estado'";
      if($id_municipio && !empty($id_municipio)){
        $sql .= " AND id_municipio = '$id_municipio'";
      }
      if($tipo && !empty($tipo)){
         $sql .= " AND id_categoria = '$tipo'";
      } 
      if($precio_minimo && !empty($precio_minimo) && $precio_maximo && !empty($precio_maximo)){
        $sql .= " AND precio BETWEEN '$precio_minimo' AND '$precio_maximo'";
      }
      $query = $this->db->query($sql." LIMIT 100");
      $result = $query->result();
      foreach($query->result() as $key=>$row){
                $fotos = $this->db->query("SELECT * FROM fotos WHERE id_vivienda = $row->id;");
                $result[$key]->fotos = $fotos->result();
      }
      return $result;
    }

    public function getViviendaById($id){
      $this->db->where('id', $id);
      $query = $this->db->get('vivienda');
      return $query->row();
    }

    public function getViviendaDetail($id){
      $query= $this->db->query("SELECT * FROM vivienda WHERE id = $id");
      $result = $query->result();
      foreach($query->result() as $key=>$row){
                $fotos = $this->db->query("SELECT * FROM fotos WHERE id_vivienda = $row->id;");
                $result[$key]->fotos = $fotos->result();
      }
      return $result;
    }


    public function getViviendaDetail2($id){
      $query= $this->db->query("SELECT v.id, v.titulo, v.precio, m.nombre as municipio, d.nombre as departamento, v.direccion, v.area_total,v.area_construida, v.cantidad_cuartos, v.tipo_piso, v.energia_electrica, v.agua, v.tipo_techo, v.cantidad_banios, v.terraza, v.cocina_amueblada, v.cochera, v.jardin, v.estado, v.id_categoria, v.is_negociable FROM vivienda v JOIN municipio m ON m.id = v.id_municipio JOIN departamento d ON d.id = m.id_departamento WHERE v.id = $id");
      $result = $query->result();
      foreach($query->result() as $key=>$row){
                $fotos = $this->db->query("SELECT * FROM fotos WHERE id_vivienda = $row->id;");
                $result[$key]->fotos = $fotos->result();
      }
      return $result;
    }

    public function getFotosVivienda($id){
      $this->db->where('id_vivienda', $id);
      $query = $this->db->get('fotos');
      return $query->result();
    }

    public function getMunicipios(){
        $query = $this->db->get('municipio');
        return $query->result();
    }

    public function getMunicipiosByDepartamento($id_departamento){
      $this->db->where('id_departamento', $id_departamento);
      $query = $this->db->get('municipio');
      return $query->result();

    }

    public function getDepartamentoByMunicipio($id_municipio){
      $query = $this->db->query("SELECT DISTINCT departamento.id FROM departamento JOIN municipio ON municipio.id_departamento = departamento.id WHERE municipio.id = '$id_municipio'");
      return $query->result();
    }

    public function getDepartamentos(){
      $query = $this->db->get('departamento');
      return $query->result();
    }

    public function getCategorias(){
        $query = $this->db->get('categoria');
        return $query->result();
    }

    public function getDetalle($param){
      $query = $this->db->query("SELECT DISTINCT v.id, v.titulo, v.precio, m.nombre as municipio, v.direccion, v.area_total FROM vivienda v JOIN municipio m ON m.id = v.id_municipio WHERE v.id like '%$param%' OR v.titulo like '%$param%' OR m.nombre like '%$param%' OR v.direccion like '%$param%' OR v.area_total like '%$param%';");
      return $query->result();
    }

    public function eliminar($id){
      $response = $this->db->delete('vivienda', array('id' => $id)); 
      return $response;
    }


}
?>