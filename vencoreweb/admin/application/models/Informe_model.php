<?php class Informe_model extends CI_Model {
	 


	public function __construct(){
    	// Call the CI_Model constructor
        parent::__construct();
    }
 

    public function getInformePorFechas($fecha1, $fecha2){
      $query = $this->db->query("SELECT DISTINCT vivienda.encargado, vivienda.numero, vivienda.poligono, pago.cuota, pago.mes_cancelado, pago.year_cancelado , pago.fecha FROM vivienda JOIN pago ON pago.id_vivienda = vivienda.id where date(pago.fecha) between '$fecha1' and '$fecha2'");
      return $query->result();
    }

     public function getInformePorCasa($numero, $poligono){
      $query = $this->db->query("SELECT DISTINCT vivienda.encargado, vivienda.numero, vivienda.poligono, pago.cuota, pago.mes_cancelado, pago.year_cancelado, pago.fecha FROM vivienda JOIN pago ON pago.id_vivienda = vivienda.id where vivienda.numero='$numero' AND vivienda.poligono='$poligono' LIMIT 20");
      return $query->result();
    }

    public function getInformePorPoligono($fecha1, $fecha2, $poligono){
      $query = $this->db->query("SELECT DISTINCT vivienda.encargado, vivienda.numero, vivienda.poligono, pago.cuota, pago.mes_cancelado, pago.year_cancelado, pago.fecha FROM vivienda JOIN pago ON pago.id_vivienda = vivienda.id where date(pago.fecha) between '$fecha1' and '$fecha2' and vivienda.poligono='$poligono'");
      return $query->result();
    }

}
?>