<?php class Usuario_model extends CI_Model {
	public $usuario;
  public $clave;
  public $nombres;
  public $apellidos;
  public $perfil;

	public function __construct(){
    	// Call the CI_Model constructor
        parent::__construct();
    }
   
  public function agregar($usuario, $clave, $nombres, $apellidos, $id_perfil){
      $this->usuario = $usuario;
      $this->clave = sha1($clave);
      $this->nombres = $nombres;
      $this->apellidos = $apellidos;
      $this->perfil = $id_perfil;
      $this->db->insert('usuario', $this);
      $id_usuario = $this->db->insert_id();
      if($this->db->affected_rows() > 0){
            return "ok";
      }else{
            return "error";
      }
  }

   public function getPerfiles(){
      $query = $this->db->get('perfil');
      return $query->result();
    }
 

    public function getUsuario($value){

      $this->db->like('usuario', $value);
      $this->db->or_like('nombres', $value);
      $this->db->or_like('apellidos', $value);
      $this->db->from('usuario'); 
      $query = $this->db->get();
      return $query->result();
    }

    public function getUsuarioById($id){
      $query = $this->db->query("SELECT usuario.id, usuario.usuario, usuario.nombres, 
        usuario.apellidos, usuario.perfil FROM usuario WHERE usuario.id = $id;");
      return $query->result();
    }

    public function eliminar($id){
      $response = $this->db->delete('usuario', array('id' => $id)); 
      return $response;
    }

    public function editar($id, $nombres, $apellidos, $perfil){
      $data = array(
               'nombres' => $nombres,
               'apellidos' => $apellidos,
               'perfil' => $perfil
            );
      $this->db->where('id', $id);  
      $this->db->update('usuario', $data); 
    }

    public function findUsuario($usuario, $clave){
      $query = $this->db->query("SELECT usuario, nombres, apellidos, perfil FROM usuario WHERE usuario='".$usuario."' AND clave='".sha1($clave)."';");
      return $query->row();
    }

}
?>